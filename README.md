# README #

Access (mdb) with Java8 Spring Hibernate by UCanAccess JDBC driver


* It is a simple project to create a DB MySQL from DB Access (.mdb). Have used some Java 8 new features, Spring 4 with Hibernate 4 . The project shows how to use two data sources in the configuration file of the Spring and also you can see Hibernate work with the Access MDB file using the UCanAccess JDBC driver 
* 1.0

Feel free to report any errors or better use of technology, such as use of stream java8 Spring or Hibernate configuration