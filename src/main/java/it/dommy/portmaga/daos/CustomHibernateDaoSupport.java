package it.dommy.portmaga.daos;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

/**
 * Created by dommy on 7/16/15.
 */
public class CustomHibernateDaoSupport extends HibernateDaoSupport
{

    @Autowired SessionFactory secondSessionFactory;
    boolean first=true;
    public SessionFactory getSecondSessionFactory(){
        if(first) {
            setSessionFactory(secondSessionFactory);
            first=false;
        }
        return  getSessionFactory();
    }

}
