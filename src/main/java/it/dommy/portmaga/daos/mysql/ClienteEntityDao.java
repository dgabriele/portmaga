package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.ClienteEntity;

/**
 * Created by dommy on 8/9/15.
 */
public interface ClienteEntityDao extends Dao<ClienteEntity>{
    ClienteEntity findByProperty(String property, String value);
}
