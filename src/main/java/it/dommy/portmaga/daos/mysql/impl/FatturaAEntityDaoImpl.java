package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.FatturaAEntityDao;
import it.dommy.portmaga.model.mysql.FatturaAEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

/**
 * Created by dommy on 10/24/15.
 */
@Repository("fatturaAEntityDao")
public class FatturaAEntityDaoImpl extends AbstractDao<FatturaAEntity> implements FatturaAEntityDao {
    @Override
    protected Class<FatturaAEntity> getModelClass() {
        return FatturaAEntity.class;
    }

    @Override
    @Transactional
    public FatturaAEntity findFatturaEntityByKey(final int codicefornitore, final String numero,
                                                 final Date data, final String tipo){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("codicefornitore",codicefornitore))
        .add(Restrictions.eq("numero", numero))
        .add(Restrictions.eq("tipo",tipo)).add(Restrictions.eq("data",data));
       return findUniqueResultByCriteria(criteria);
    }

    @Override
    @Transactional
    public FatturaAEntity findFatturaByCodiceAcquisto(final Long codiceAcquisto){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("codiceAcquisto",codiceAcquisto));
        return findUniqueResultByCriteria(criteria);
    }
}
