package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.ProdottiMDao;
import it.dommy.portmaga.model.mysql.FatturaEntity;
import it.dommy.portmaga.model.mysql.ProdottoEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 7/17/15.
 */
@Repository("prodottiMDao")
public class ProdottiMDaoImpl extends AbstractDao<ProdottoEntity> implements ProdottiMDao {

    @Override
    protected Class<ProdottoEntity> getModelClass() {
        return ProdottoEntity.class;
    }

    @Transactional
    @Override
    public ProdottoEntity findByProperty(final String property, final String value) {
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq(property, value));
        ProdottoEntity prodottoEntity= (ProdottoEntity) criteria.uniqueResult();
        return  prodottoEntity;
    }
}
