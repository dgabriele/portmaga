package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.DocumentoVenditaEntityDao;
import it.dommy.portmaga.model.mysql.DocumentoAcquistoEntity;
import it.dommy.portmaga.model.mysql.DocumentoAcquistoEntity_;
import it.dommy.portmaga.model.mysql.DocumentoVenditaEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

/**
 * Created by dommy on 8/14/15.
 */
@Repository("documentoVenditaEntityDao")
public class DocumentoVenditaEntityDaoImpl extends AbstractDao<DocumentoVenditaEntity> implements DocumentoVenditaEntityDao {
    @Override
    protected Class<DocumentoVenditaEntity> getModelClass() {
        return DocumentoVenditaEntity.class;
    }

    @Override
    @Transactional
    public DocumentoVenditaEntity findDocumentoByNumeroAndData(final String numero,final Date data){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("numero",numero));
        criteria.add(Restrictions.eq("data",data));
        return findUniqueResultByCriteria(criteria);
    }
}
