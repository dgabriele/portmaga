package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.PagamentoFornitoreEntityDao;
import it.dommy.portmaga.model.mysql.PagamentoFornitoreEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 10/30/15.
 */
@Repository("pagamentoFornitoreEntityDao")
public class PagamentoFornitoreEntityDaoImpl extends AbstractDao<PagamentoFornitoreEntity> implements PagamentoFornitoreEntityDao {
    @Override
    protected Class<PagamentoFornitoreEntity> getModelClass() {
        return PagamentoFornitoreEntity.class;
    }
}
