package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.FatturaAEntity;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

/**
 * Created by dommy on 10/24/15.
 */
public interface FatturaAEntityDao extends Dao<FatturaAEntity> {
    @Transactional
    FatturaAEntity findFatturaEntityByKey(int codicefornitore, String numero,
                                          Date data, String tipo);

    @Transactional
    FatturaAEntity findFatturaByCodiceAcquisto(Long codiceAcquisto);
}
