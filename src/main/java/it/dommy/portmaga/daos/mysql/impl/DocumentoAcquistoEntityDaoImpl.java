package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.DocumentoAcquistoEntityDao;
import it.dommy.portmaga.model.mysql.DocumentoAcquistoEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 10/28/15.
 */
@Repository("documentoAcquistoEntityDao")
public class DocumentoAcquistoEntityDaoImpl extends AbstractDao<DocumentoAcquistoEntity> implements DocumentoAcquistoEntityDao {
    @Override
    protected Class<DocumentoAcquistoEntity> getModelClass() {
        return DocumentoAcquistoEntity.class;
    }

    @Override
    @Transactional
    public DocumentoAcquistoEntity findByCodiceAcquisto(final Long codiceAcquisto){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("codiceAcquisto", codiceAcquisto));
        return findUniqueResultByCriteria(criteria);
    }
}
