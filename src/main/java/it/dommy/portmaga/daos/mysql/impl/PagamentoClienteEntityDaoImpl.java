package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.PagamentoClienteEntityDao;
import it.dommy.portmaga.model.mysql.PagamentoClienteEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 8/14/15.
 */
@Repository("pagamentoClienteEntityDao")
public class PagamentoClienteEntityDaoImpl extends AbstractDao<PagamentoClienteEntity> implements PagamentoClienteEntityDao {
    @Override
    protected Class<PagamentoClienteEntity> getModelClass() {
        return PagamentoClienteEntity.class;
    }
}
