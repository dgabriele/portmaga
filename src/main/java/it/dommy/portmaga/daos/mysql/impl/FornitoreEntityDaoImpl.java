package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.FornitoreEntityDao;
import it.dommy.portmaga.model.mysql.FornitoreEntity;
import org.hibernate.Criteria;
import org.hibernate.NonUniqueResultException;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 10/24/15.
 */
@Repository("fornitoreEntityDao")
public class FornitoreEntityDaoImpl extends AbstractDao<FornitoreEntity> implements FornitoreEntityDao {

    @Override
    protected Class<FornitoreEntity> getModelClass() {
        return FornitoreEntity.class;
    }

    @Override
    @Transactional
    public FornitoreEntity findCodiceFornitoreByCodiceFInterno(final String codiceFornitoreInterno)throws NonUniqueResultException {
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("codiceFornitoreInterno", codiceFornitoreInterno));
        return findUniqueResultByCriteria(criteria);
    }
}
