package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.FornitoreEntity;
import org.hibernate.NonUniqueResultException;

/**
 * Created by dommy on 10/24/15.
 */
public interface FornitoreEntityDao extends Dao<FornitoreEntity> {
    FornitoreEntity findCodiceFornitoreByCodiceFInterno(String codiceFornitoreInterno)throws NonUniqueResultException;
}
