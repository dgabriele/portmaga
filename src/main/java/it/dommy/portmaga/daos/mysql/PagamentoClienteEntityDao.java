package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.PagamentoClienteEntity;

/**
 * Created by dommy on 8/14/15.
 */
public interface PagamentoClienteEntityDao extends Dao<PagamentoClienteEntity> {
}
