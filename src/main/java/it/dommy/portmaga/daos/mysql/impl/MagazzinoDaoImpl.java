package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.MagazzinoDao;
import it.dommy.portmaga.model.mysql.Magazzino;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 8/7/15.
 */
@Repository("magazzinoDao")
public class MagazzinoDaoImpl extends AbstractDao<Magazzino> implements MagazzinoDao {
    @Override
    protected Class<Magazzino> getModelClass() {
        return Magazzino.class;
    }
}
