package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.TotaleFatturaAEntity;

/**
 * Created by dommy on 10/27/15.
 */
public interface TotaleFatturaAEntityDao extends Dao<TotaleFatturaAEntity> {
}
