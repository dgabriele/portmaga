package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.FornitoriCodiciDao;
import it.dommy.portmaga.model.mysql.FornitoriCodiciEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by dommy on 12/8/15.
 */
@Repository("fornitoriCodiciDao")
public class FornitoriCodiciDaoImpl extends AbstractDao<FornitoriCodiciEntity> implements FornitoriCodiciDao {

    @Override
    protected Class<FornitoriCodiciEntity> getModelClass() {
        return FornitoriCodiciEntity.class;
    }

    @Override
    public FornitoriCodiciEntity findByCodiceAndCodDitta(String codice,String codiceDitta) {
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("codice", codice));
        criteria.add(Restrictions.eq("codiceDitta", codiceDitta));
        List<FornitoriCodiciEntity> lista=criteria.list();
        if(!lista.isEmpty())
            return  lista.get(0);
        return  null;
    }
}
