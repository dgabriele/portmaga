package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.DettaglioFatturaEntityDao;
import it.dommy.portmaga.model.mysql.DettaglioFatturaEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 8/14/15.
 */
@Repository("dettaglioFatturaEntityDao")
public class DettaglioFatturaEntityDaoImpl extends AbstractDao<DettaglioFatturaEntity> implements DettaglioFatturaEntityDao {
    @Override
    protected Class<DettaglioFatturaEntity> getModelClass() {
        return DettaglioFatturaEntity.class;
    }
}
