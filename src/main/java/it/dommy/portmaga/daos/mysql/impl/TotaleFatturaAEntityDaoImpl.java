package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.TotaleFatturaAEntityDao;
import it.dommy.portmaga.model.mysql.TotaleFatturaAEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 10/27/15.
 */
@Repository("totaleFatturaAEntityDao")
public class TotaleFatturaAEntityDaoImpl extends AbstractDao<TotaleFatturaAEntity> implements TotaleFatturaAEntityDao {

    @Override
    protected Class<TotaleFatturaAEntity> getModelClass() {
        return TotaleFatturaAEntity.class;
    }
}
