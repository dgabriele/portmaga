package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.PagamentoFornitoreEntity;

/**
 * Created by dommy on 10/30/15.
 */
public interface PagamentoFornitoreEntityDao extends Dao<PagamentoFornitoreEntity> {
}
