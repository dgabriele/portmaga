package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.DettaglioFatturaAEntityDao;
import it.dommy.portmaga.model.mysql.DettaglioFatturaAEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 10/25/15.
 */

@Repository("dettaglioFatturaAEntityDao")
public class DettaglioFatturaAEntityDaoImpl extends AbstractDao<DettaglioFatturaAEntity> implements DettaglioFatturaAEntityDao {

    @Override
    protected Class<DettaglioFatturaAEntity> getModelClass() {
        return DettaglioFatturaAEntity.class;
    }
}
