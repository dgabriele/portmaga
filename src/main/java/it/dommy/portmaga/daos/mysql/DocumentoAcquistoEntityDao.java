package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.DocumentoAcquistoEntity;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 10/28/15.
 */
public interface DocumentoAcquistoEntityDao extends Dao<DocumentoAcquistoEntity> {
    @Transactional
    DocumentoAcquistoEntity findByCodiceAcquisto(Long codiceAcquisto);
}
