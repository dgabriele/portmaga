package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.DocumentoVenditaEntity;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;

/**
 * Created by dommy on 8/14/15.
 */
public interface DocumentoVenditaEntityDao extends Dao<DocumentoVenditaEntity> {
    @Transactional
    DocumentoVenditaEntity findDocumentoByNumeroAndData(String numero, Date data);
}
