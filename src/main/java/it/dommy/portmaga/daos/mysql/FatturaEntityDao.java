package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.FatturaEntity;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/**
 * Created by dommy on 8/14/15.
 */
public interface FatturaEntityDao extends Dao<FatturaEntity> {

    @Transactional
    FatturaEntity findByNumeroAndData(Integer value, Date data);
}
