package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.DettaglioFatturaAEntity;
import it.dommy.portmaga.model.mysql.DettaglioFatturaEntity;

/**
 * Created by dommy on 10/25/15.
 */
public interface DettaglioFatturaAEntityDao extends Dao<DettaglioFatturaAEntity> {
}
