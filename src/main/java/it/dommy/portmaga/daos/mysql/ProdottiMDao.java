package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.ProdottoEntity;
import org.springframework.transaction.annotation.Transactional;

public interface ProdottiMDao extends Dao<ProdottoEntity> {
    @Transactional
    ProdottoEntity findByProperty(String property, String value);
}