package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.TotaleFatturaEntityDao;
import it.dommy.portmaga.model.mysql.TotaleFatturaEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 8/14/15.
 */
@Repository("totaleFatturaEntityDao")
public class TotaleFatturaEntityDaoImpl extends AbstractDao<TotaleFatturaEntity> implements TotaleFatturaEntityDao {
    @Override
    protected Class<TotaleFatturaEntity> getModelClass() {
        return TotaleFatturaEntity.class;
    }
}
