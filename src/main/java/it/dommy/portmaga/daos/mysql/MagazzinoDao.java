package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.Magazzino;

/**
 * Created by dommy on 8/7/15.
 */
public interface MagazzinoDao extends Dao<Magazzino> {
}
