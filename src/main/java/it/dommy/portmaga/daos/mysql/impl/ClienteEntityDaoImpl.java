package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.ClienteEntityDao;
import it.dommy.portmaga.model.mysql.ClienteEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 8/9/15.
 */
@Repository("clienteEntityDao")
public class ClienteEntityDaoImpl extends AbstractDao<ClienteEntity> implements ClienteEntityDao{
    @Override
    protected Class<ClienteEntity> getModelClass() {
        return ClienteEntity.class;
    }

    @Transactional
    @Override
    public ClienteEntity findByProperty(final String property, final String value) {
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq(property, value));
        ClienteEntity clienteEntity= (ClienteEntity) criteria.uniqueResult();
        return  clienteEntity;
    }
}
