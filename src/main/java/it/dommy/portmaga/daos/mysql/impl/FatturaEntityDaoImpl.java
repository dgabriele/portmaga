package it.dommy.portmaga.daos.mysql.impl;

import it.dommy.portmaga.daos.AbstractDao;
import it.dommy.portmaga.daos.mysql.FatturaEntityDao;
import it.dommy.portmaga.model.mysql.ClienteEntity;
import it.dommy.portmaga.model.mysql.FatturaEntity;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/**
 * Created by dommy on 8/14/15.
 */
@Repository("fatturaEntityDao")
public class FatturaEntityDaoImpl extends AbstractDao<FatturaEntity> implements FatturaEntityDao {
    @Override
    protected Class<FatturaEntity> getModelClass() {
        return FatturaEntity.class;
    }

    @Transactional
    @Override
    public FatturaEntity findByNumeroAndData(final Integer value, final Date data) {
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("numero", value));
        criteria.add(Restrictions.eq("data",data));
        FatturaEntity fatturaEntity= (FatturaEntity) criteria.uniqueResult();
        return  fatturaEntity;
    }
}
