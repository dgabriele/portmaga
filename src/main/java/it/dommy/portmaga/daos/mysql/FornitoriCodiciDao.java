package it.dommy.portmaga.daos.mysql;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.mysql.FornitoriCodiciEntity;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 12/8/15.
 */
public interface FornitoriCodiciDao extends Dao<FornitoriCodiciEntity> {
    @Transactional
    public FornitoriCodiciEntity findByCodiceAndCodDitta(String codice,String codiceDitta);
}
