package it.dommy.portmaga.daos;

import it.dommy.portmaga.model.Model;

import java.util.List;

public interface Dao<T extends Model> {

    T save(final T entity);
    
    T update(final T entity) throws Exception ;

    void delete(final T entity);

    List<T> findAll();

    List<T> findByExample(final T entity);
    
    
     
}
