package it.dommy.portmaga.daos;


import it.dommy.portmaga.model.Model;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.NonUniqueResultException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import java.util.List;
import java.util.Set;

public abstract class AbstractAccessDao<T extends Model> extends CustomHibernateDaoSupport implements Dao<T> {

	private static final Logger LOGGER = Logger.getLogger(AbstractAccessDao.class.getName());
	protected final byte VALIDITA_DEFAULT=1;
	/**
	 * This method is overridden by each subclass. It returns back the
	 * particular Entity that the subclass is responsible for.
	 *
	 * @return Class<T> Entity on which the the subclass is reponsible for.
	 */
	protected abstract Class<T> getModelClass();

	/**
	 * Persist entity to the database.
	 *
	 * @param entity to persist.
	 */
/*
 * @Transactional
 * .If the calling method is already in a transaction it will use the same transaction 
If the calling method is in a non-transaction scope it will create new transaction .
Example, two classes A and B. A is calling a method on B. B's method is marked at REQUIRED. 
If A has already started a Transaction, then B will just join that Transaction and run within it. So if A or B causes the transaction to rollback, everything will rollback. 
If A has not started a Transaction, then the call to B will create a new Transaction and all the code in B will be within the Transaction, but all the code in A is running outside of a transaction.*/
	
	
	@Transactional
	public T save(final T entity) {
		try {
			getSecondSessionFactory().getCurrentSession().saveOrUpdate(entity);
			LOGGER.debug("Saved {" +getModelClass().getSimpleName()+"} entity with Class {"+ entity.getClass().toString()+"}");
		} catch (final javax.validation.ConstraintViolationException e) {
			final Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
			LOGGER.error("While saving {"+ entity.getClass().getSimpleName()+"} with Class {"+ entity.getClass().toString()+"} there were {"+ constraintViolations.size()+"} constraint violations. Details to follow");
			for (final ConstraintViolation<?> constraintViolation : constraintViolations) {
				LOGGER.error( constraintViolation.toString());
			}
			throw e;
		} catch (final org.hibernate.exception.ConstraintViolationException e) {
			LOGGER.error("While saving {"+entity.getClass().getSimpleName()+"} with Class {"+ entity.getClass().toString()+"} there were a constraint violation on {"+ e.getConstraintName()+"}");
			throw e;
		}
		return entity;
	}

	@Transactional
	public T update(final T entity) throws Exception {
		try {
			getSecondSessionFactory().getCurrentSession().update(entity);
			LOGGER.debug("Update  {" +getModelClass().getSimpleName()+"} entity with Class {"+ entity.getClass().toString()+"}");
		} catch (final javax.validation.ConstraintViolationException e) {
			final Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
			LOGGER.error("While update {"+ entity.getClass().getSimpleName()+"} with Class {"+ entity.getClass().toString()+"} there were {"+ constraintViolations.size()+"} constraint violations. Details to follow");
			for (final ConstraintViolation<?> constraintViolation : constraintViolations) {
				LOGGER.error( constraintViolation.toString());
			}
			throw (new Exception());
		} catch (final org.hibernate.exception.ConstraintViolationException e) {
			LOGGER.error("While update {"+entity.getClass().getSimpleName()+"} with Class {"+ entity.getClass().toString()+"} there were a constraint violation on {"+ e.getConstraintName()+"}");
			throw (new Exception());
		}
		return entity;
	}

	/**
	 * Delete entity from the database.
	 *
	 * @param entity to delete.
	 */

	@Transactional
	public void delete(final T entity) {
		getSecondSessionFactory().getCurrentSession().delete(entity);
		// LOGGER.debug("Deleted {} entity with Class {}", getModelClass().getSimpleName(), entity.getClass().toString());
	}


	/**
	 * Find all Entities existing in the database.
	 *
	 * @return List<T> of Entities. If no entities are found, return an empty
	 *         list.
	 */

	@Transactional
	public List<T> findAll() {
		final List<T> allEntities = findByCriteria();
		LOGGER.debug("Found all ({"+ allEntities.size()+"}) {"+ getModelClass().getSimpleName()+"} entities");
		return allEntities;

	}

	/**
	 * Find all Entities depending on whatever criteria has been set.
	 *
	 * @param criterion that should be used to narrow the search.
	 * @return List<T> of Entities. If no entities are found, return an empty
	 *         list.
	 */
	@Transactional
	protected List<T> findByCriteria(final Criterion... criterion) {
		final Criteria criteria = createCriteria();
		for (final Criterion c : criterion) {
			criteria.add(c);
		}
		return findByCriteria(criteria);
	}

	/**
	 * Find single Entity depending on whatever criteria has been set.
	 *
	 * @param criterion that should be used to narrow the search.
	 * @return the found entity, or null if none found
	 * @throws NonUniqueResultException if there is more than one matching
	 *             result
	 */
	protected T findUniqueResultByCriteria(final Criterion... criterion) throws NonUniqueResultException {
		final Criteria criteria = createCriteria();
		for (final Criterion c : criterion) {
			criteria.add(c);
		}
		return findUniqueResultByCriteria(criteria);
	}

	/**
	 * Create a new <tt>Criteria</tt> instance. To be used in other DAO classes
	 * to create complex object-based queries.
	 *
	 * @return a new <tt>Criteria</tt> instance.
	 */
	@Transactional
	protected Criteria createCriteria() {

		return getSecondSessionFactory().getCurrentSession().createCriteria(getModelClass());
	}

	/**
	 * Find all Entities from the given criteria.
	 *
	 * @param criteria the criteria for the find.
	 * @return List<T> of Entities. If no entities are found, return an empty
	 *         list.
	 */
	@Transactional
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(final Criteria criteria) throws HibernateException{
		final List<T> filteredEntities = criteria.list();
		LOGGER.debug("Found {"+ filteredEntities.size()+"} {"+getModelClass().getSimpleName()+"} entities");
		return filteredEntities;
	}

	/**
	 * Find single Entity from the given criteria.
	 *
	 * @param criteria the criteria for the find
	 * @return the found entity, or null if none found
	 * @throws NonUniqueResultException if there is more than one matching
	 *             result
	 */
	@SuppressWarnings("unchecked")
	protected T findUniqueResultByCriteria(final Criteria criteria) throws NonUniqueResultException {
		final T uniqueResult = (T)criteria.uniqueResult();
		LOGGER.debug("Found unique result for {"+getModelClass().getSimpleName()+"} entity");
		return uniqueResult;
	}

	/**
	 * Find all Entities from a given Example with default excludes.<br/>
	 * <br/>
	 * By default, the excludes are creationDate and lastModifiedDate
	 *
	 * @param entity to use as the Example.
	 * @return List<T> of entities. If no entities are found, return an empty
	 *         list.
	 */

	public List<T> findByExample(final T entity) {
		final Example example = Example.create(entity);
		addDefaultConfigurations(example);
		final String[] excluded = addDefaultExcludeProperties(example);
		final Criteria criteria = createCriteria();
		criteria.add(example);
		final List<T> filteredEntities = findByCriteria(criteria);
		final Object[] logArgs = {filteredEntities.size(), getModelClass().getSimpleName(), excluded};
		LOGGER.debug("Found {"+logArgs+"} {} entities, excluding {} fields");
		return filteredEntities;
	}

	/**
	 * Adds the default configurations to the specified example.
	 *
	 * @param example the {@link Example} object ton which to add the
	 *            configurations
	 */
	protected void addDefaultConfigurations(final Example example) {
		example.ignoreCase().enableLike();
	}

	/**
	 * Adds the default exclude properties to the specified example, and passes
	 * back the excluded properties.
	 *
	 * @param example the {@link Example} object on which to add the exclude
	 *            properties
	 * @return the excludes properties
	 */
	protected String[] addDefaultExcludeProperties(final Example example) {
		final String[] excluded = {"creationDate", "lastModifiedDate"};
		for (final String exclude : excluded) {
			example.excludeProperty(exclude);
			example.excludeProperty(exclude);
		}
		return excluded;
	}

}
