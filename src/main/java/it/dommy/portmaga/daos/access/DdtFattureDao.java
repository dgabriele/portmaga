package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.DdtFatture;

import java.util.Date;

/**
 * Created by dommy on 10/27/15.
 */
public interface DdtFattureDao extends Dao<DdtFatture> {
    DdtFatture findDdtByNumeroAndData(String numero, Date date);
}
