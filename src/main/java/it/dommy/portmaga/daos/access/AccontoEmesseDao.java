package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.AccontoEmesse;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

/**
 * Created by dommy on 11/8/15.
 */
public interface AccontoEmesseDao extends Dao<AccontoEmesse> {
    @Transactional
    List<AccontoEmesse> findByCodiceFattura(final String codiceFattura,final Date data);
}
