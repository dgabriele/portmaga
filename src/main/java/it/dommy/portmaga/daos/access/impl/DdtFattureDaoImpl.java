package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.DdtFattureDao;
import it.dommy.portmaga.model.access.DdtFatture;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by dommy on 10/27/15.
 */
@Repository("ddtFattureDao")
public class DdtFattureDaoImpl extends AbstractAccessDao<DdtFatture> implements DdtFattureDao {

    @Override
    protected Class<DdtFatture> getModelClass() {
        return DdtFatture.class;
    }

    @Override
    @Transactional
    public DdtFatture findDdtByNumeroAndData(final String numero,final Date date){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("numero",numero))
        .add(Restrictions.eq("data",date));
        return findUniqueResultByCriteria(criteria);
    }
}
