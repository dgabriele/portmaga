package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.ProdottiDao;
import it.dommy.portmaga.model.access.Prodotti;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 7/15/15.
 */
@Repository("prodottiDao")
public class ProdottiDaoImpl extends AbstractAccessDao<Prodotti> implements ProdottiDao{


    @Override
    protected Class<Prodotti> getModelClass() {

        return Prodotti.class;
    }
}