package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.ClientiDao;
import it.dommy.portmaga.model.access.Clienti;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 8/9/15.
 */
@Repository("clientiDao")
public class ClientiDaoImpl extends AbstractAccessDao<Clienti> implements ClientiDao {
    @Override
    protected Class<Clienti> getModelClass() {
        return Clienti.class;
    }
}
