package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.Fatture;

/**
 * Created by dommy on 8/12/15.
 */
public interface FattureDao extends Dao<Fatture>{
}
