package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.FattureDao;
import it.dommy.portmaga.model.access.Fatture;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 8/12/15.
 */

@Repository("fattureDao")
public class FattureDaoImpl extends AbstractAccessDao<Fatture> implements FattureDao {
    @Override
    protected Class<Fatture> getModelClass() {
        return Fatture.class;
    }
}
