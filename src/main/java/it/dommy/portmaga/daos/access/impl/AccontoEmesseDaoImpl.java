package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.AccontoEmesseDao;
import it.dommy.portmaga.model.access.AccontoAcquisto;
import it.dommy.portmaga.model.access.AccontoEmesse;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.List;

/**
 * Created by dommy on 11/8/15.
 */
@Repository("accontoEmesseDao")
public class AccontoEmesseDaoImpl extends AbstractAccessDao<AccontoEmesse> implements AccontoEmesseDao {
    @Override
    protected Class<AccontoEmesse> getModelClass() {
        return AccontoEmesse.class;
    }

    @Override
    @Transactional
    public List<AccontoEmesse> findByCodiceFattura(final String codiceFattura,final Date data){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("fattura", codiceFattura));
        criteria.add(Restrictions.eq("datafattura",data));
        return  findByCriteria(criteria);
    }
}
