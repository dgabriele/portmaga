package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.Fornitori;

/**
 * Created by dommy on 10/24/15.
 */
public interface FornitoriDao extends Dao<Fornitori>{
}
