package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.AccontoAcquisto;

import java.util.List;

/**
 * Created by dommy on 10/29/15.
 */
public interface AccontoAcquistoDao extends Dao<AccontoAcquisto> {
    public List<AccontoAcquisto> findByCodiceAcquisto(Long codiceAcuisto);
}
