package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.AccontoAcquistoDao;
import it.dommy.portmaga.model.access.AccontoAcquisto;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dommy on 10/29/15.
 */
@Repository("accontoAcquistoDao")
public class AccontoAcquistoDaoImpl extends AbstractAccessDao<AccontoAcquisto> implements AccontoAcquistoDao {

    @Override
    protected Class<AccontoAcquisto> getModelClass() {
        return AccontoAcquisto.class;
    }

    @Override
    @Transactional
    public List<AccontoAcquisto> findByCodiceAcquisto(final Long codiceAcuisto){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("fattura", codiceAcuisto));
        return  findByCriteria(criteria);
    }
}
