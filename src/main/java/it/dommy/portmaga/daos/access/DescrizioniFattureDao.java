package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.DescrizioniFatture;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by dommy on 8/12/15.
 */
public interface DescrizioniFattureDao extends Dao<DescrizioniFatture> {
    List<DescrizioniFatture> listDescrizioniFattureByNumeroAndData(String numero, Date date);

    List<DescrizioniFatture> findAllOrderByNumeroAndData();
}
