package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.DescrizioniFattureDao;
import it.dommy.portmaga.model.access.DescrizioniFatture;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.crypto.dsig.Transform;
import java.util.Date;
import java.util.List;

/**
 * Created by dommy on 8/12/15.
 */
@Repository("descrizioniFattureDao")
public class DescrizioniFattureDaoImpl extends AbstractAccessDao<DescrizioniFatture> implements DescrizioniFattureDao {

    @Override
    protected Class<DescrizioniFatture> getModelClass() {
        return DescrizioniFatture.class;
    }

    @Override
    @Transactional
    public List<DescrizioniFatture> listDescrizioniFattureByNumeroAndData(String numero, Date date){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("numero", numero));
        criteria.add(Restrictions.eq("data", date));
        criteria.setResultTransformer(Criteria.ROOT_ENTITY);
        return criteria.list();


    }

    @Override
    @Transactional
    public List<DescrizioniFatture> findAllOrderByNumeroAndData() {
//        Criteria criteria=createCriteria();
//        criteria.addOrder(Order.asc("data"));
////        criteria.addOrder(Order.asc("numero"));
//        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
//        return criteria.list();
        return getSecondSessionFactory().getCurrentSession()
                .createSQLQuery("select * from DESCRIZIONIFATTURE order by data asc,numero asc")
                .addScalar("numero")
                .addScalar("data")
                .addScalar("descrizione")
                .addScalar("codice")
                .addScalar("quantita")
                .addScalar("prezzo")
                .addScalar("importo")
                .addScalar("misura")
                .addScalar("iva")
                .addScalar("sconto")
                .addScalar("provvigione")
                .addScalar("stornati")
                .addScalar("lotto")
                .addScalar("posizione")
                .setResultTransformer(Transformers.aliasToBean(DescrizioniFatture.class)).list();

    }
}
