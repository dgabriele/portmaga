package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.Clienti;

/**
 * Created by dommy on 8/9/15.
 */
public interface ClientiDao extends Dao<Clienti>{
}
