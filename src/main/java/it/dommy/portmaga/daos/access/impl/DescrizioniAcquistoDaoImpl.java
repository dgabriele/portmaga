package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.DescrizioniAcquistoDao;
import it.dommy.portmaga.model.access.DescrizioniAcquisto;
import it.dommy.portmaga.model.access.DescrizioniFatture;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by dommy on 10/25/15.
 */
@Repository("descrizioniAcquistoDao")
public class DescrizioniAcquistoDaoImpl extends AbstractAccessDao<DescrizioniAcquisto> implements DescrizioniAcquistoDao {

    @Override
    protected Class<DescrizioniAcquisto> getModelClass() {
        return DescrizioniAcquisto.class;
    }

    @Override
    @Transactional
    public List<DescrizioniAcquisto> findAllDescrizioniAcqOrderByCodAndNum(){
//        Criteria criteria=createCriteria();
//        criteria.addOrder(Order.asc("data"));
//        criteria.addOrder(Order.asc("codiceAcquisto"));
//
//        return criteria.list();

        return getSecondSessionFactory().getCurrentSession()
                .createSQLQuery("select * from DESCRIZIONIACQUISTO order by data asc,codiceAcquisto asc")
                .addScalar("codice")
                .addScalar("descrizione")
                .addScalar("quantita")
                .addScalar("prezzo")
                .addScalar("codiceAcquisto")
                .addScalar("data")
                .setResultTransformer(Transformers.aliasToBean(DescrizioniAcquisto.class)).list();
    }
}
