package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.Prodotti;

/**
 * Created by dommy on 7/15/15.
 */
public interface ProdottiDao extends Dao<Prodotti> {

}
