package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.FornitoriDao;
import it.dommy.portmaga.model.access.Fornitori;
import org.springframework.stereotype.Repository;

/**
 * Created by dommy on 10/24/15.
 */
@Repository("fornitoriDao")
public class FornitoriDaoImpl extends AbstractAccessDao<Fornitori> implements FornitoriDao {

    @Override
    protected Class<Fornitori> getModelClass() {
        return Fornitori.class;
    }
}
