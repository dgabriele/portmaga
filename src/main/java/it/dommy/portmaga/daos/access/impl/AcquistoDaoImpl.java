package it.dommy.portmaga.daos.access.impl;

import it.dommy.portmaga.daos.AbstractAccessDao;
import it.dommy.portmaga.daos.access.AcquistoDao;
import it.dommy.portmaga.model.access.Acquisto;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 10/24/15.
 */
@Repository("acquistoDao")
public class AcquistoDaoImpl extends AbstractAccessDao<Acquisto> implements AcquistoDao {

    @Override
    protected Class<Acquisto> getModelClass() {
        return Acquisto.class;
    }

    @Override
    @Transactional
    public Acquisto findByCodiceAcquisto(final long codiceAcquisto){
        Criteria criteria=createCriteria();
        criteria.add(Restrictions.eq("codiceAcquisto", codiceAcquisto));
        return  findUniqueResultByCriteria(criteria);
    }
}
