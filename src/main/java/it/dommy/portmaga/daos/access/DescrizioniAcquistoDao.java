package it.dommy.portmaga.daos.access;

import it.dommy.portmaga.daos.Dao;
import it.dommy.portmaga.model.access.DescrizioniAcquisto;

import java.util.List;

/**
 * Created by dommy on 10/25/15.
 */
public interface DescrizioniAcquistoDao extends Dao<DescrizioniAcquisto> {
    List<DescrizioniAcquisto> findAllDescrizioniAcqOrderByCodAndNum();
}
