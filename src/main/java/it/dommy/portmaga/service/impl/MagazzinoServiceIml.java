package it.dommy.portmaga.service.impl;

import it.dommy.portmaga.daos.mysql.MagazzinoDao;
import it.dommy.portmaga.model.mysql.Magazzino;
import it.dommy.portmaga.service.MagazzinoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by dommy on 8/7/15.
 */
@Service("magazzinoService")
public class MagazzinoServiceIml implements MagazzinoService {

    MagazzinoDao magazzinoDao;

    @Override
    @Transactional
    public void createMagazzino() {
        Magazzino magazzino=new Magazzino();
        magazzino.setDescrizione("Magazzino Principale");
        magazzinoDao.save(magazzino);
    }

    public MagazzinoDao getMagazzinoDao() {
        return magazzinoDao;
    }

    public void setMagazzinoDao(MagazzinoDao magazzinoDao) {
        this.magazzinoDao = magazzinoDao;
    }
}
