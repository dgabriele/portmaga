package it.dommy.portmaga.service.impl;

import it.dommy.portmaga.daos.access.ClientiDao;
import it.dommy.portmaga.daos.mysql.ClienteEntityDao;
import it.dommy.portmaga.model.access.Clienti;
import it.dommy.portmaga.model.mysql.ClienteEntity;
import it.dommy.portmaga.service.ClientiService;
import it.dommy.portmaga.util.Utils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by dommy on 8/9/15.
 */

@Service("clientiService")
public class ClientiServiceImpl implements ClientiService {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ClientiServiceImpl.class.getName());

    private ClientiDao clientiDao;
    private ClienteEntityDao clienteEntityDao;

    @Override
    public void copyClientiToClienteEntity() {
        LOGGER.debug("copyClientiToClienteEntity start");
        List<Clienti> listClienti= clientiDao.findAll();
        List<ClienteEntity> listClienteEntity=listClienti.stream().map(clienteEntityToClienti).collect(Collectors.toList());
        listClienteEntity.forEach(clienteEntity -> clienteEntityDao.save(clienteEntity));
        LOGGER.debug("copyClientiToClienteEntity stop");

    }

    Function<Clienti,ClienteEntity> clienteEntityToClienti =new Function<Clienti,ClienteEntity>() {

        @Override
        public ClienteEntity apply(Clienti clienti) {
            ClienteEntity clienteEntity=new ClienteEntity();
            clienteEntity.setRagionesociale(Utils.replaceApiceClienti(Utils.trimInitEndSpace(clienti.getRagioneSociale())));
            Utils.containApice("clienti getRagioneSociale", clienteEntity.getRagionesociale());
            clienteEntity.setCap(Utils.trimInitEndSpace(Utils.extractCap(clienti.getCittà())));
            clienteEntity.setCitta(Utils.trimInitEndSpace(Utils.replaceApiceClienti(Utils.extractCity(clienti.getCittà()))));
            Utils.containApice("clienti getCittà", clienteEntity.getCitta());
            clienteEntity.setProvincia(Utils.trimInitEndSpace(Utils.replaceApiceClienti(Utils.extractProvince(clienti.getCittà()))));
            Utils.containApice("clienti getProvincia", clienteEntity.getProvincia());
            clienteEntity.setVia(Utils.trimInitEndSpace(Utils.replaceApiceClienti(clienti.getIndirizzo())));
            Utils.containApice("clienti getVia", clienteEntity.getVia());
            clienteEntity.setTelefono1(Utils.trimInitEndSpace(Utils.subStringTelefono(clienti.getTelefono())));
            clienteEntity.setCf(Utils.trimInitEndSpace(clienti.getCodicefiscale()));
            clienteEntity.setPi(Utils.trimInitEndSpace(clienti.getPartitaiva()));
            clienteEntity.setModalitapagamento(Utils.trimInitEndSpace(Utils.replaceApiceClienti(clienti.getTipopagamento())));
            Utils.containApice("clienti getTipopagamento", clienteEntity.getModalitapagamento());
            clienteEntity.setCodiceClienteInterno(Utils.trimInitEndSpace(clienti.getCodicecliente()));
            clienteEntity.setListino("" + clienti.getListino());
            clienteEntity.setSconto(Utils.trimInitEndSpace(clienti.getSconto()));
            return clienteEntity;
        }
    };

    public ClienteEntityDao getClienteEntityDao() {
        return clienteEntityDao;
    }

    public void setClienteEntityDao(ClienteEntityDao clienteEntityDao) {
        this.clienteEntityDao = clienteEntityDao;
    }

    public ClientiDao getClientiDao() {
        return clientiDao;
    }

    public void setClientiDao(ClientiDao clientiDao) {
        this.clientiDao = clientiDao;
    }
}
