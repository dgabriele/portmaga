package it.dommy.portmaga.service.impl;

import it.dommy.portmaga.daos.access.ProdottiDao;
import it.dommy.portmaga.daos.mysql.MagazzinoDao;
import it.dommy.portmaga.daos.mysql.ProdottiMDao;
import it.dommy.portmaga.model.access.Prodotti;
import it.dommy.portmaga.model.mysql.Magazzino;
import it.dommy.portmaga.model.mysql.ProdottoEntity;
import it.dommy.portmaga.service.ProdottiService;
import it.dommy.portmaga.util.Utils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by dommy on 7/16/15.
 */
@Service("prodottiService")
public class ProdottiServiceImpl implements ProdottiService {
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ProdottiServiceImpl.class.getName());

    ProdottiDao prodottiDao;
    ProdottiMDao prodottiMDao;
    MagazzinoDao magazzinoDao;
    Magazzino magazzinoEntity;

    public void copyProdotti() {
        LOGGER.debug("copyProdotti start");
        magazzinoEntity =magazzinoDao.findAll().get(0);
        List<Prodotti> listProdotti= prodottiDao.findAll();
        List<ProdottoEntity> listProdottiM= listProdotti.stream().map(prodottiToProdottiM).collect(Collectors.toList());
        listProdottiM.forEach(prodottiM -> prodottiMDao.save(prodottiM));
        LOGGER.debug("copyProdotti end");
    }

    Function<Prodotti,ProdottoEntity> prodottiToProdottiM=new Function<Prodotti, ProdottoEntity>() {
        @Override
        public ProdottoEntity apply(Prodotti prodotti) {
            ProdottoEntity prodottoEntity=new ProdottoEntity();
            BigDecimal iva=prodotti.getIva();
            prodottoEntity.setCodiceprodottointerno(prodotti.getCodice());
            prodottoEntity.setCodiceprodottofornitore(prodotti.getCodiceditta());
            prodottoEntity.setDescrizione(Utils.replaceApiceProdotti(Utils.trimInitEndSpace(prodotti.getDescrizione())));
            Utils.containApice("Prodotti Descrizione", prodottoEntity.getDescrizione());
            prodottoEntity.setUmacquisto(Utils.replaceMisura(prodotti.getMisura()));
            prodottoEntity.setUmvendita(Utils.replaceMisura(prodotti.getMisura()));
            prodottoEntity.setAliquota(iva);
            prodottoEntity.setPrezzoacquisto(prodotti.getPrezzo());
            prodottoEntity.setPrezzo(prodotti.getListino1());
            prodottoEntity.setPrezzoiva(Utils.percentage(prodotti.getListino1(), prodotti.getIva()));
//            prodottoEntity.setListino1(prodotti.getListino1());
//            prodottoEntity.setListino1Iva(Utils.percentage(prodotti.getListino1(), prodotti.getIva()));
            prodottoEntity.setListino1(prodotti.getListino2());
            prodottoEntity.setListino1Iva(Utils.percentage(prodotti.getListino2(), prodotti.getIva()));
            if(!(prodotti.getPrezzo().compareTo(BigDecimal.ZERO) == 0)) {
                prodottoEntity.setRicarico1(Utils.getRicarico(prodotti.getPrezzo(), prodotti.getListino1()));
                prodottoEntity.setRicarico2(Utils.getRicarico(prodotti.getPrezzo(), prodotti.getListino2()));
            }
            prodottoEntity.setEsistenza(prodotti.getGiacenza());
            prodottoEntity.setUbicazione(Utils.trimInitEndSpace(prodotti.getUbicazione()));
            prodottoEntity.setScortasicurezza(prodotti.getScorta());
            prodottoEntity.setTipologia(Utils.trimInitEndSpace(prodotti.getCategoria()));
            prodottoEntity.setMagazzino(magazzinoEntity);
            return prodottoEntity;
        }
    };
    public ProdottiDao getProdottiDao() {
        return prodottiDao;
    }

    public void setProdottiDao(ProdottiDao prodottiDao) {
        this.prodottiDao = prodottiDao;
    }

    public ProdottiMDao getProdottiMDao() {
        return prodottiMDao;
    }

    public void setProdottiMDao(ProdottiMDao prodottiMDao) {
        this.prodottiMDao = prodottiMDao;
    }

    public MagazzinoDao getMagazzinoDao() {
        return magazzinoDao;
    }

    public void setMagazzinoDao(MagazzinoDao magazzinoDao) {
        this.magazzinoDao = magazzinoDao;
    }
}
