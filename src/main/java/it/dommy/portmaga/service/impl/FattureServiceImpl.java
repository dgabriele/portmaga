package it.dommy.portmaga.service.impl;

import it.dommy.portmaga.daos.access.AccontoEmesseDao;
import it.dommy.portmaga.daos.access.DescrizioniFattureDao;
import it.dommy.portmaga.daos.access.FattureDao;
import it.dommy.portmaga.daos.mysql.*;
import it.dommy.portmaga.model.access.AccontoEmesse;
import it.dommy.portmaga.model.access.DescrizioniFatture;
import it.dommy.portmaga.model.access.Fatture;
import it.dommy.portmaga.model.mysql.*;
import it.dommy.portmaga.service.FattureService;
import it.dommy.portmaga.util.Utils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by dommy on 8/14/15.
 */
@Service("fattureService")
public class FattureServiceImpl implements FattureService {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(FattureServiceImpl.class.getName());

    private FattureDao fattureDao;
    private DescrizioniFattureDao descrizioniFattureDao;
    private FatturaEntityDao fatturaEntityDao;
    private DettaglioFatturaEntityDao dettaglioFatturaEntityDao;
    private TotaleFatturaEntityDao totaleFatturaEntityDao;
    private ClienteEntityDao clienteEntityDao;
    private ProdottiMDao prodottiMDao;
    private DocumentoVenditaEntityDao documentoVenditaEntityDao;
    private PagamentoClienteEntityDao pagamentoClienteEntityDao;
    private AccontoEmesseDao accontoEmesseDao;

    @Override
    public void copyFatture() {
        LOGGER.debug("copyFatture START");
//        Parte Fattura
        List<Fatture> listFattures=fattureDao.findAll();
        List<FatturaEntity> listFatturaEntities=listFattures.stream().map(fattureFatturaEntityFunction).collect(Collectors.toList());
        List<TotaleFatturaEntity> listTotaleFatturaEntities= IntStream.range(0, listFattures.size())
                .mapToObj(i -> copyTotateFatture.apply(listFattures.get(i), listFatturaEntities.get(i)))
                .collect(Collectors.toList());
        listFatturaEntities.stream().forEach((entity) -> fatturaEntityDao.save(entity));
        listTotaleFatturaEntities.stream().forEach(totaleFatturaEntity -> totaleFatturaEntityDao.save(totaleFatturaEntity));

//      Parte Descrizione
        List<DescrizioniFatture> listDescrizioniFattures=descrizioniFattureDao.findAllOrderByNumeroAndData();

        List<DettaglioFatturaEntity> listDettaglioFatturaEntities=listDescrizioniFattures.stream()
                .map(descrizioniFattureDettaglioFatturaEntityFunction)
                .collect(Collectors.toList());
        listDettaglioFatturaEntities.stream().forEach(dettaglioFatturaEntity -> dettaglioFatturaEntityDao.save(dettaglioFatturaEntity));

//        Parte Documento Vendita
        List<DocumentoVenditaEntity> listDocumentoVenditaEntities=listFattures.stream().map(fatturaToDocumentoVenditaEntityFunction).collect(Collectors.toList());
        listDocumentoVenditaEntities.stream().forEach(documentoVenditaEntity -> documentoVenditaEntityDao.save(documentoVenditaEntity));
//        Parte  Pagamento Cliente
        List<PagamentoClienteEntity> listPagamentoClienteEntities= IntStream.range(0,listFattures.size())
                .mapToObj(value -> pagamentoClienteBFunction.apply(listFattures.get(value),listDocumentoVenditaEntities.get(value))).collect(Collectors.toList());
        List<PagamentoClienteEntity> listPagamentoClienteEntitiesFilter=listPagamentoClienteEntities.stream().filter(p -> p.getDocumentoVenditaEntity() != null).collect(Collectors.toList());
//       Elimino le pagate
        List<Fatture> listFatturesNoPagate= listFattures.stream().filter(fatture -> !fatture.isPagato()).collect(Collectors.toList());
        for (Fatture fatture : listFatturesNoPagate) {
            List<AccontoEmesse> listAccontoEmesses=accontoEmesseDao.findByCodiceFattura(fatture.getNumero(),fatture.getData());
            List<PagamentoClienteEntity> listPagamento=listAccontoEmesses.stream().map(pagamentoDocumentoVendita).collect(Collectors.toList());
            listPagamentoClienteEntitiesFilter.addAll(listPagamento);
        }
        listPagamentoClienteEntitiesFilter.forEach(pag->pagamentoClienteEntityDao.save(pag));

    }


    /**
     * Funzione che mappa Fatture
     */
    private Function<Fatture,FatturaEntity> fattureFatturaEntityFunction =(fatture)-> {

        FatturaEntity fatturaEntity =new FatturaEntity();
        ClienteEntity clienteEntity =clienteEntityDao.findByProperty("codiceClienteInterno", fatture.getCliente());
        fatturaEntity.setClienteEntity(clienteEntity);
        fatturaEntity.setNumero(Integer.parseInt(fatture.getNumero()));
        fatturaEntity.setNumerazione(fatture.getNumero());
        fatturaEntity.setData(fatture.getData());
        fatturaEntity.setScperc(new BigDecimal(fatture.getImportosconto()));
        fatturaEntity.setTipo(fatture.isDaddt() ? "F.D.V." : "F.I.V.");
        fatturaEntity.setDestinazione(clienteEntity.getVia());
        fatturaEntity.setCausale(Utils.trimInitEndSpace(fatture.getCausale()));
        fatturaEntity.setTrasporto(Utils.replaceTrasporto(fatture.getTipotrasporto()));
        fatturaEntity.setAspetto(Utils.trimInitEndSpace(fatture.getAspetto()));
        fatturaEntity.setNumerocolli(String.valueOf(fatture.getColli()));
        fatturaEntity.setPorto(Utils.trimInitEndSpace(fatture.getPorto()));
        fatturaEntity.setDataritiro(fatture.getDataTrasporto());
        fatturaEntity.setModalitapagamento(Utils.trimInitEndSpace(fatture.getTipopagamento()));
        fatturaEntity.setListino(String.valueOf(fatture.getListino()));
        fatturaEntity.setNote(Utils.trimInitEndSpace(fatture.getNote()));
        return fatturaEntity;
    };

    /**
     * Funzione che mappa TottaleFatture
     */
    private BiFunction<Fatture,FatturaEntity,TotaleFatturaEntity> copyTotateFatture =(fatture, fatturaEntity) -> {

        TotaleFatturaEntity totaleFatturaEntity=new TotaleFatturaEntity();

        totaleFatturaEntity.setFatturaEntity(fatturaEntity);
        totaleFatturaEntity.setClienteEntity(fatturaEntity.getClienteEntity());

        List<DescrizioniFatture> listDescrizione=descrizioniFattureDao.listDescrizioniFattureByNumeroAndData(fatture.getNumero(), fatture.getData());


        double imponibile1= listDescrizione.stream()
                .filter(d -> d.getIva().equalsIgnoreCase("4"))
                .mapToDouble(d -> d.getImporto())
                .sum();

        double imponibile2= listDescrizione.stream()
                .filter(d->d.getIva().equalsIgnoreCase("10"))
                .mapToDouble(d->d.getImporto())
                .sum();

        double imponibile3= listDescrizione.stream()
                .filter(d -> d.getIva().trim().startsWith("2"))
                .mapToDouble(d -> d.getImporto())
                .sum();

        int iva2=Integer.parseInt(listDescrizione.stream()
                .filter(d -> d.getIva().startsWith("2"))
                .map(d -> d.getIva())
                .findFirst()
                .orElse("0"));

        totaleFatturaEntity.setImponibile1(new BigDecimal(imponibile1));
        totaleFatturaEntity.setImposta1(new BigDecimal((imponibile1 * 4) / 100));
        totaleFatturaEntity.setAliquota1(new BigDecimal(4));
        totaleFatturaEntity.setEsente(new BigDecimal(0));
        totaleFatturaEntity.setImponibile2(new BigDecimal(imponibile2));
        totaleFatturaEntity.setImposta2(new BigDecimal((imponibile2 * 10) / 100));
        totaleFatturaEntity.setAliquota2(new BigDecimal(10));

        totaleFatturaEntity.setImponibile3(new BigDecimal(imponibile3));
        totaleFatturaEntity.setImposta3(new BigDecimal((imponibile3 * iva2) / 100));

        totaleFatturaEntity.setAliquota3(imponibile3 > 0 ? new BigDecimal(iva2) : new BigDecimal(22));

        totaleFatturaEntity.setImponibile(new BigDecimal(fatture.getImponibile()));
        totaleFatturaEntity.setImposta(new BigDecimal(fatture.getImposta()));
        totaleFatturaEntity.setScperc(new BigDecimal(0));
        totaleFatturaEntity.setImponibiles(new BigDecimal(fatture.getImponibile()));
        totaleFatturaEntity.setSconto(new BigDecimal(0));
        totaleFatturaEntity.setTotale(new BigDecimal(fatture.getTotale()));

        return totaleFatturaEntity;
    };

    private Function<DescrizioniFatture,DettaglioFatturaEntity> descrizioniFattureDettaglioFatturaEntityFunction=(descrizioniFatture) -> {

        DettaglioFatturaEntity dettaglioFatturaEntity=new DettaglioFatturaEntity();
        FatturaEntity fatturaEntity=fatturaEntityDao.findByNumeroAndData(Integer.parseInt(descrizioniFatture.getNumero()), descrizioniFatture.getData());
        ClienteEntity clienteEntity =fatturaEntity.getClienteEntity();
        ProdottoEntity prodottoEntity=prodottiMDao.findByProperty("codiceprodottointerno", descrizioniFatture.getCodice());

        dettaglioFatturaEntity.setFatturaEntity(fatturaEntity);
        dettaglioFatturaEntity.setClienteEntity(clienteEntity);
        if(prodottoEntity!=null) {
            dettaglioFatturaEntity.setProdottoEntity(prodottoEntity);
            dettaglioFatturaEntity.setCodiceprodottointerno(prodottoEntity.getCodiceprodottointerno());
            Magazzino magazzino = prodottoEntity.getMagazzino();
            dettaglioFatturaEntity.setMagazzino(magazzino);
        }
        if(descrizioniFatture.getDescrizione()!=null || descrizioniFatture.getDescrizione()!="" || !descrizioniFatture.getDescrizione().contains("Rif. ddt")) {
            dettaglioFatturaEntity.setQuantita(new BigDecimal(descrizioniFatture.getQuantita()));
            dettaglioFatturaEntity.setAliquota(Utils.isInteger(descrizioniFatture.getIva()) ? new BigDecimal(descrizioniFatture.getIva()) : new BigDecimal(0));
        }
        dettaglioFatturaEntity.setUm(Utils.trimInitEndSpace(descrizioniFatture.getMisura()));
        dettaglioFatturaEntity.setDescrizione(Utils.trimInitEndSpace(Utils.replaceApiceProdotti(descrizioniFatture.getDescrizione())));
        Utils.containApice("dettaglioFatturaEntity getDescrizione", dettaglioFatturaEntity.getDescrizione());
        dettaglioFatturaEntity.setSconto(Utils.trimInitEndSpace(descrizioniFatture.getSconto()));
        dettaglioFatturaEntity.setPrezzo(new BigDecimal(descrizioniFatture.getPrezzo()));
        if(descrizioniFatture.getQuantita()!=0) {
            double prezzos = descrizioniFatture.getImporto() / descrizioniFatture.getQuantita();
            dettaglioFatturaEntity.setPrezzos(new BigDecimal(prezzos));
        }
        dettaglioFatturaEntity.setImporto(new BigDecimal(descrizioniFatture.getImporto()));

        return  dettaglioFatturaEntity;
    };

    private Function<Fatture,DocumentoVenditaEntity> fatturaToDocumentoVenditaEntityFunction=(fatture) ->{

        DocumentoVenditaEntity documentoVenditaEntity =new DocumentoVenditaEntity();
        ClienteEntity clienteEntity = clienteEntityDao.findByProperty("codiceClienteInterno", fatture.getCliente());
        documentoVenditaEntity.setClienteEntity(clienteEntity);
        documentoVenditaEntity.setNumero(fatture.getNumero());
        documentoVenditaEntity.setData(fatture.getData());
        documentoVenditaEntity.setMese(Utils.getMonth(fatture.getData()));
        documentoVenditaEntity.setAnno(Utils.getYear(fatture.getData()));
        documentoVenditaEntity.setTipo(fatture.isDaddt() ? "F.D.V." : "F.I.V.");
        documentoVenditaEntity.setIdentificativodocumento(documentoVenditaEntity.getTipo().equalsIgnoreCase("F.D.V.") ? "Fattura Differita" : "Fattura Immediata");
        documentoVenditaEntity.setImponibile(new BigDecimal(fatture.getImponibile()));
        documentoVenditaEntity.setImposta(new BigDecimal(fatture.getImposta()));
        documentoVenditaEntity.setTotale(new BigDecimal(fatture.getTotale()));
        documentoVenditaEntity.setNumeroprogressivo(Integer.parseInt(fatture.getNumero()));
        documentoVenditaEntity.setModalitapagamento(Utils.getMezzo(fatture.getPagamento()));
        documentoVenditaEntity.setEsente(new BigDecimal(0));
        documentoVenditaEntity.setImportoscadenza(new BigDecimal(0));
        documentoVenditaEntity.setImportoinsoluto(new BigDecimal(0));
        Utils.containApice("documentoVenditaEntity getModalitapagamento", documentoVenditaEntity.getModalitapagamento());
        documentoVenditaEntity.setAcconto(fatture.isPagato() ? new BigDecimal(fatture.getTotale()) : new BigDecimal(fatture.getAcconto()));
//        documentoVenditaEntity.setSaldo(new BigDecimal(fatture.getAcconto()));
        documentoVenditaEntity.setNote(Utils.replaceApiceClienti(fatture.getNote()));
        Utils.containApice("documentoVenditaEntity getNote", documentoVenditaEntity.getNote());

        return  documentoVenditaEntity;
    };

    public BiFunction<Fatture,DocumentoVenditaEntity,PagamentoClienteEntity> pagamentoClienteBFunction=(fatture, documentoVenditaEntity) ->{

        PagamentoClienteEntity pagamentoClienteEntity=new PagamentoClienteEntity();
        if(fatture.isPagato()) {
            pagamentoClienteEntity.setDocumentoVenditaEntity(documentoVenditaEntity);
            pagamentoClienteEntity.setClienteEntity(documentoVenditaEntity.getClienteEntity());
            pagamentoClienteEntity.setMezzopagamento(Utils.getMezzo(fatture.getPagamento()));
            pagamentoClienteEntity.setDatapagamento(fatture.getDatapagamento());
            pagamentoClienteEntity.setNumerodocumento(documentoVenditaEntity.getNumero());
            pagamentoClienteEntity.setDatadocumento(documentoVenditaEntity.getData());
            pagamentoClienteEntity.setTipodocumento(documentoVenditaEntity.getTipo());
            pagamentoClienteEntity.setImporto(new BigDecimal(fatture.getTotale()));
            pagamentoClienteEntity.setData(fatture.getData());
        }
        return pagamentoClienteEntity;
    } ;

    public Function<AccontoEmesse,PagamentoClienteEntity> pagamentoDocumentoVendita=(accontoEmesse)->{
        PagamentoClienteEntity pagamentoClienteEntity=new PagamentoClienteEntity();
        DocumentoVenditaEntity documentoVenditaEntity=documentoVenditaEntityDao.findDocumentoByNumeroAndData(accontoEmesse.getFattura(), accontoEmesse.getDatafattura());
        pagamentoClienteEntity.setDocumentoVenditaEntity(documentoVenditaEntity);
        pagamentoClienteEntity.setClienteEntity(documentoVenditaEntity.getClienteEntity());
        pagamentoClienteEntity.setMezzopagamento(Utils.getMezzo(accontoEmesse.getTipo()));
        pagamentoClienteEntity.setDatapagamento(accontoEmesse.getData());
        pagamentoClienteEntity.setNumerodocumento(documentoVenditaEntity.getNumero());
        pagamentoClienteEntity.setDatadocumento(documentoVenditaEntity.getData());
        pagamentoClienteEntity.setTipodocumento(documentoVenditaEntity.getTipo());
        pagamentoClienteEntity.setImporto(new BigDecimal(accontoEmesse.getImporto()));
        pagamentoClienteEntity.setData(accontoEmesse.getData());
        return pagamentoClienteEntity;
    };
    public FattureDao getFattureDao() {
        return fattureDao;
    }

    public void setFattureDao(FattureDao fattureDao) {
        this.fattureDao = fattureDao;
    }

    public DescrizioniFattureDao getDescrizioniFattureDao() {
        return descrizioniFattureDao;
    }

    public void setDescrizioniFattureDao(DescrizioniFattureDao descrizioniFattureDao) {
        this.descrizioniFattureDao = descrizioniFattureDao;
    }

    public FatturaEntityDao getFatturaEntityDao() {
        return fatturaEntityDao;
    }

    public void setFatturaEntityDao(FatturaEntityDao fatturaEntityDao) {
        this.fatturaEntityDao = fatturaEntityDao;
    }

    public DettaglioFatturaEntityDao getDettaglioFatturaEntityDao() {
        return dettaglioFatturaEntityDao;
    }

    public void setDettaglioFatturaEntityDao(DettaglioFatturaEntityDao dettaglioFatturaEntityDao) {
        this.dettaglioFatturaEntityDao = dettaglioFatturaEntityDao;
    }

    public TotaleFatturaEntityDao getTotaleFatturaEntityDao() {
        return totaleFatturaEntityDao;
    }

    public void setTotaleFatturaEntityDao(TotaleFatturaEntityDao totaleFatturaEntityDao) {
        this.totaleFatturaEntityDao = totaleFatturaEntityDao;
    }

    public ClienteEntityDao getClienteEntityDao() {
        return clienteEntityDao;
    }

    public void setClienteEntityDao(ClienteEntityDao clienteEntityDao) {
        this.clienteEntityDao = clienteEntityDao;
    }

    public ProdottiMDao getProdottiMDao() {
        return prodottiMDao;
    }

    public void setProdottiMDao(ProdottiMDao prodottiMDao) {
        this.prodottiMDao = prodottiMDao;
    }

    public DocumentoVenditaEntityDao getDocumentoVenditaEntityDao() {
        return documentoVenditaEntityDao;
    }

    public void setDocumentoVenditaEntityDao(DocumentoVenditaEntityDao documentoVenditaEntityDao) {
        this.documentoVenditaEntityDao = documentoVenditaEntityDao;
    }

    public PagamentoClienteEntityDao getPagamentoClienteEntityDao() {
        return pagamentoClienteEntityDao;
    }

    public void setPagamentoClienteEntityDao(PagamentoClienteEntityDao pagamentoClienteEntityDao) {
        this.pagamentoClienteEntityDao = pagamentoClienteEntityDao;
    }

    public void setAccontoEmesseDao(AccontoEmesseDao accontoEmesseDao) {
        this.accontoEmesseDao = accontoEmesseDao;
    }
}
