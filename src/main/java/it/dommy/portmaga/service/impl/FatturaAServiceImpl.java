package it.dommy.portmaga.service.impl;

import it.dommy.portmaga.daos.access.AccontoAcquistoDao;
import it.dommy.portmaga.daos.access.AcquistoDao;
import it.dommy.portmaga.daos.access.DdtFattureDao;
import it.dommy.portmaga.daos.access.DescrizioniAcquistoDao;
import it.dommy.portmaga.daos.mysql.*;
import it.dommy.portmaga.model.access.AccontoAcquisto;
import it.dommy.portmaga.model.access.Acquisto;
import it.dommy.portmaga.model.access.DdtFatture;
import it.dommy.portmaga.model.access.DescrizioniAcquisto;
import it.dommy.portmaga.model.mysql.*;
import it.dommy.portmaga.service.FatturaAService;
import it.dommy.portmaga.util.Utils;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by dommy on 10/24/15.
 */
@Service("fatturaAService")
public class FatturaAServiceImpl implements FatturaAService {

    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(FatturaAServiceImpl.class.getName());

    private FatturaAEntityDao fatturaAEntityDao;
    private AcquistoDao acquistoDao;
    private FornitoreEntityDao fornitoreEntityDao;
    private DescrizioniAcquistoDao descrizioniAcquistoDao;
    private DettaglioFatturaAEntityDao dettaglioFatturaAEntityDao;
    private ProdottiMDao prodottiMDao;
    private DdtFattureDao ddtFattureDao;
    private TotaleFatturaAEntityDao totaleFatturaAEntityDao;
    private DocumentoAcquistoEntityDao documentoAcquistoEntityDao;
    private AccontoAcquistoDao accontoAcquistoDao;
    private PagamentoFornitoreEntityDao pagamentoFornitoreEntityDao;
    private FornitoriCodiciDao fornitoriCodiciDao;

    @Override
    public void copyFattureAcquisto(){
        LOGGER.debug("copyFattureAcquisto START");
//        Sezione Acquisti
        List<Acquisto> listAcquistos=acquistoDao.findAll();
        List<Acquisto> listAcquistosNotDDt=listAcquistos.stream().filter(acquisto -> acquisto.isFattura()).collect(Collectors.toList());
        List<FatturaAEntity> listFatturaAEntity=listAcquistosNotDDt.stream()
                .map(copyAcquistoToFatturaA)
                .collect(Collectors.toList());
        listFatturaAEntity.stream().forEach(fatturaAEntity -> fatturaAEntityDao.save(fatturaAEntity));
//        Parte Descrizioni
        List<DescrizioniAcquisto> listDescrizioniAcquistos=descrizioniAcquistoDao.findAllDescrizioniAcqOrderByCodAndNum();
        List<DettaglioFatturaAEntity> listDettaglioFatturaAEntities=listDescrizioniAcquistos.stream()
                .map(copyDescAcquiToDettaglioFatt).collect(Collectors.toList());
        listDettaglioFatturaAEntities.stream()
                .filter(dettaglioFatturaAEntity1 -> dettaglioFatturaAEntity1.getNumero() != null)
                .forEach(dettaglioFatturaAEntity -> dettaglioFatturaAEntityDao.save(dettaglioFatturaAEntity));
//      Parte TotaleFatturaAEntity
        List<TotaleFatturaAEntity> listTotaleFatturaAEntities= IntStream.range(0, listAcquistosNotDDt.size())
                .mapToObj(i -> copyTotateFattureA.apply(listAcquistosNotDDt.get(i), listFatturaAEntity.get(i)))
                .collect(Collectors.toList());
        listTotaleFatturaAEntities.stream().forEach(totaleFatturaEntity -> totaleFatturaAEntityDao.save(totaleFatturaEntity));
//      Parte Documento Acquisto
        List<DocumentoAcquistoEntity> listDocumentoAcquistoEntities=listAcquistosNotDDt.stream()
                .map(fatturaToDocumentoAcquistoEntityFunction).collect(Collectors.toList());
        listDocumentoAcquistoEntities.stream().forEach(documentoAcquistoEntity -> documentoAcquistoEntityDao.save(documentoAcquistoEntity));
////      Parte Pagamento Fornitore
        List<PagamentoFornitoreEntity> listPagamentoFornitoreEntities=listDocumentoAcquistoEntities.stream()
                .map(pagamentoClienteBFunction)
                .collect(Collectors.toList());
//        Elimino le fatture che non erano pagate e quindi bisogna aggiungere gli acconti
        List<PagamentoFornitoreEntity> listPagamentoFornitoreEntitiesNoEmpty=listPagamentoFornitoreEntities
                .stream()
                .filter(pagamentoFornitoreEntity -> pagamentoFornitoreEntity.getIddocumento()!=0)
                .collect(Collectors.toList());
//        Elimino i pagati
        List<Acquisto> listAcquistoNoPagato=listAcquistosNotDDt.
                stream().filter(acquisto1 -> !acquisto1.isPagato()).collect(Collectors.toList());
        for (Acquisto acquisto : listAcquistoNoPagato) {
                List<AccontoAcquisto> listAccontoAcquisto = accontoAcquistoDao.findByCodiceAcquisto(acquisto.getCodiceAcquisto());
                List<PagamentoFornitoreEntity> listPagame = listAccontoAcquisto.stream().map(accontoAcquistoToPagamenti).collect(Collectors.toList());
                listPagamentoFornitoreEntitiesNoEmpty.addAll(listPagame);
        }
        listPagamentoFornitoreEntitiesNoEmpty.stream().forEach(pagamentoFornitoreEntity -> pagamentoFornitoreEntityDao.save(pagamentoFornitoreEntity));

    }


    private Function<Acquisto,FatturaAEntity> copyAcquistoToFatturaA=acquisto -> {
        FatturaAEntity fatturaAEntity=new FatturaAEntity();
        FornitoreEntity fornitoreEntity= fornitoreEntityDao.findCodiceFornitoreByCodiceFInterno(acquisto.getFornitore());
        fatturaAEntity.setCodicefornitore(fornitoreEntity.getCodicefornitore());
        fatturaAEntity.setNumero(Utils.trimInitEndSpace(acquisto.getNumero()));
        fatturaAEntity.setData(acquisto.getData());
        fatturaAEntity.setModalitapagamento(Utils.replaceApiceProdotti(Utils.trimInitEndSpace(acquisto.getTipoPagamento())));
        fatturaAEntity.setTipo("F.I.A.");
        fatturaAEntity.setCodiceAcquisto(acquisto.getCodiceAcquisto());
        return fatturaAEntity;
    };

    private Function<DescrizioniAcquisto,DettaglioFatturaAEntity> copyDescAcquiToDettaglioFatt=descrizioniAcquisto -> {
        DettaglioFatturaAEntity dettaglioFatturaAEntity=new DettaglioFatturaAEntity();
        Acquisto acquisto=acquistoDao.findByCodiceAcquisto(descrizioniAcquisto.getCodiceAcquisto());
        FatturaAEntity fatturaAEntity=fatturaAEntityDao.findFatturaByCodiceAcquisto(descrizioniAcquisto.getCodiceAcquisto());
        ProdottoEntity prodottoEntity=prodottiMDao.findByProperty("codiceprodottointerno", descrizioniAcquisto.getCodice());

        if(fatturaAEntity==null && !acquisto.isFattura()){
            DdtFatture ddtFatture=ddtFattureDao.findDdtByNumeroAndData(acquisto.getNumero(),acquisto.getData());
            if(ddtFatture!=null) {
                fatturaAEntity = fatturaAEntityDao.findFatturaByCodiceAcquisto(ddtFatture.getCodiceAcquisto());
                acquisto=acquistoDao.findByCodiceAcquisto(ddtFatture.getCodiceAcquisto());
            }
        }
        if(fatturaAEntity!=null) {
            dettaglioFatturaAEntity.setCodicefornitore(fatturaAEntity.getCodicefornitore());
            dettaglioFatturaAEntity.setTipo(fatturaAEntity.getTipo());
            dettaglioFatturaAEntity.setData(fatturaAEntity.getData());
            dettaglioFatturaAEntity.setNumero(Utils.trimInitEndSpace(fatturaAEntity.getNumero()));
            if (prodottoEntity != null) {
                dettaglioFatturaAEntity.setCodiceprodotto(prodottoEntity.getCodiceprodotto());
                dettaglioFatturaAEntity.setCodicemagazzino(prodottoEntity.getMagazzino().getCodiceMagazzino());
                dettaglioFatturaAEntity.setUm(prodottoEntity.getUmacquisto());
                dettaglioFatturaAEntity.setCodiceprodottointerno(prodottoEntity.getCodiceprodottointerno());
                FornitoriCodiciEntity fornitoriCodici=fornitoriCodiciDao.findByCodiceAndCodDitta(prodottoEntity.getCodiceprodottointerno(),String.valueOf(fatturaAEntity.getCodicefornitore()));
                if(fornitoriCodici!=null)
                    dettaglioFatturaAEntity.setCodiceArticoloFornitore(Utils.trimInitEndSpace(fornitoriCodici.getCodProdDitta()));

            }
            dettaglioFatturaAEntity.setQuantita(new BigDecimal(descrizioniAcquisto.getQuantita()));

            dettaglioFatturaAEntity.setDescrizione(Utils.trimInitEndSpace(Utils.replaceApiceProdotti(descrizioniAcquisto.getDescrizione())));
            if(acquisto.getImponibile()==0)
                dettaglioFatturaAEntity.setAliquota(new BigDecimal(0));
            else {
                Double aliquotaD = (acquisto.getImposta() / acquisto.getImponibile()) * 100;
                int aliquota = (int) Math.round(aliquotaD);
                if(aliquota>10&& aliquota<19 )
                    dettaglioFatturaAEntity.setAliquota(new BigDecimal(10));
                else if (aliquota==19)
                    dettaglioFatturaAEntity.setAliquota(new BigDecimal(20));
                else
                    dettaglioFatturaAEntity.setAliquota(new BigDecimal(aliquota));

            }
            dettaglioFatturaAEntity.setPrezzo(new BigDecimal(descrizioniAcquisto.getPrezzo()));
            dettaglioFatturaAEntity.setSconto("0");
            dettaglioFatturaAEntity.setPrezzos(new BigDecimal(descrizioniAcquisto.getPrezzo()));
            dettaglioFatturaAEntity.setImporto(new BigDecimal(descrizioniAcquisto.getPrezzo()*descrizioniAcquisto.getQuantita()));
        }
        return dettaglioFatturaAEntity;
    };

    private BiFunction<Acquisto,FatturaAEntity,TotaleFatturaAEntity> copyTotateFattureA =(acquisto, fatturaAEntity) -> {
        TotaleFatturaAEntity totaleFatturaAEntity =new TotaleFatturaAEntity();
        totaleFatturaAEntity.setFatturaA(fatturaAEntity);
        totaleFatturaAEntity.setImponibile(new BigDecimal(acquisto.getImponibile()));
        totaleFatturaAEntity.setImponibiles(new BigDecimal(acquisto.getImponibile()));
        totaleFatturaAEntity.setImposta(new BigDecimal(acquisto.getImposta()));
        totaleFatturaAEntity.setSconto(new BigDecimal(0));
        totaleFatturaAEntity.setScperc(new BigDecimal(0));
        totaleFatturaAEntity.setEsente(new BigDecimal(0));
        totaleFatturaAEntity.setTotale(new BigDecimal(acquisto.getTotale()));
        Double aliquotaD = (acquisto.getImposta() / acquisto.getImponibile())*100;
        if(aliquotaD!=null) {
            Long aliquota = Math.round(aliquotaD);
            if (aliquota < 10) {
                totaleFatturaAEntity.setAliquota1(new BigDecimal(aliquota.intValue()));
                totaleFatturaAEntity.setImponibile1(totaleFatturaAEntity.getImponibile());
                totaleFatturaAEntity.setImposta1(new BigDecimal(acquisto.getImposta()));
            } else if (aliquota >= 10 && aliquota < 19) {
                totaleFatturaAEntity.setAliquota2(new BigDecimal(aliquota.intValue()));
                totaleFatturaAEntity.setImponibile2(totaleFatturaAEntity.getImponibile());
                totaleFatturaAEntity.setImposta2(new BigDecimal(acquisto.getImposta()));
            } else {
                totaleFatturaAEntity.setAliquota3(new BigDecimal(aliquota.intValue()));
                totaleFatturaAEntity.setImponibile3(totaleFatturaAEntity.getImponibile());
                totaleFatturaAEntity.setImposta3(new BigDecimal(acquisto.getImposta()));
            }
        }
        return totaleFatturaAEntity;
    };

    private Function<Acquisto,DocumentoAcquistoEntity>  fatturaToDocumentoAcquistoEntityFunction=(acquisto) ->{
        DocumentoAcquistoEntity documentoAcquistoEntity=new DocumentoAcquistoEntity();
        FornitoreEntity fornitoreEntity=fornitoreEntityDao.findCodiceFornitoreByCodiceFInterno(acquisto.getFornitore());
        documentoAcquistoEntity.setCodicefornitore(fornitoreEntity.getCodicefornitore());
        documentoAcquistoEntity.setNumero(acquisto.getNumero());
        documentoAcquistoEntity.setData(acquisto.getData());
        documentoAcquistoEntity.setMese(Utils.getMonth(acquisto.getData()));
        documentoAcquistoEntity.setAnno(Utils.getYear(acquisto.getData()));
        documentoAcquistoEntity.setTipo("F.I.A.");
        documentoAcquistoEntity.setModalitapagamento(Utils.replaceApiceProdotti(Utils.trimInitEndSpace(acquisto.getTipoPagamento())));
        documentoAcquistoEntity.setImponibile(new BigDecimal(acquisto.getImponibile()));
        documentoAcquistoEntity.setImposta(new BigDecimal(acquisto.getImposta()));
        documentoAcquistoEntity.setTotale(new BigDecimal(acquisto.getTotale()));
        documentoAcquistoEntity.setEsente(new BigDecimal(0));
        documentoAcquistoEntity.setImportoinsoluto(new BigDecimal(0));
        documentoAcquistoEntity.setImportoscadenza(new BigDecimal(0));
        documentoAcquistoEntity.setAcconto(acquisto.isPagato() ? documentoAcquistoEntity.getTotale() : new BigDecimal(acquisto.getAcconto()));
//        documentoAcquistoEntity.setSaldo(documentoAcquistoEntity.getTotale().subtract(documentoAcquistoEntity.getAcconto()));
        documentoAcquistoEntity.setImportoinsoluto(new BigDecimal(0));
        documentoAcquistoEntity.setCodiceAcquisto(acquisto.getCodiceAcquisto());
        return documentoAcquistoEntity;
    };


    private Function<DocumentoAcquistoEntity,PagamentoFornitoreEntity> pagamentoClienteBFunction=(documentoAcquistoEntity) ->{

        PagamentoFornitoreEntity pagamentoFornitoreEntity=new PagamentoFornitoreEntity();
        Acquisto acquisto=acquistoDao.findByCodiceAcquisto(documentoAcquistoEntity.getCodiceAcquisto());
        if(acquisto.isPagato()) {
            FornitoreEntity fornitoreEntity= fornitoreEntityDao.findCodiceFornitoreByCodiceFInterno(acquisto.getFornitore());
            pagamentoFornitoreEntity.setIddocumento(documentoAcquistoEntity.getIddocumento());
            pagamentoFornitoreEntity.setCodicefornitore(fornitoreEntity.getCodicefornitore());
            pagamentoFornitoreEntity.setMezzopagamento(Utils.getMezzo(acquisto.getTipoPagamento()));
            pagamentoFornitoreEntity.setDatapagamento(acquisto.getDatapagamento());
            pagamentoFornitoreEntity.setNumerodocumento(documentoAcquistoEntity.getNumero());
            pagamentoFornitoreEntity.setDatadocumento(documentoAcquistoEntity.getData());
            pagamentoFornitoreEntity.setTipodocumento(documentoAcquistoEntity.getTipo());
            pagamentoFornitoreEntity.setImporto(new BigDecimal(acquisto.getTotale()));
            pagamentoFornitoreEntity.setData(acquisto.getData());
        }

        return pagamentoFornitoreEntity;
    } ;

    private Function<AccontoAcquisto,PagamentoFornitoreEntity>  accontoAcquistoToPagamenti=(accontoAcquisto) -> {
        PagamentoFornitoreEntity pagamentoFornitoreEntity=new PagamentoFornitoreEntity();
        DocumentoAcquistoEntity documentoAcquistoEntity=documentoAcquistoEntityDao.findByCodiceAcquisto(accontoAcquisto.getFattura());
        pagamentoFornitoreEntity.setIddocumento(documentoAcquistoEntity.getIddocumento());
        pagamentoFornitoreEntity.setCodicefornitore(documentoAcquistoEntity.getCodicefornitore());
        pagamentoFornitoreEntity.setDatapagamento(accontoAcquisto.getData());
        pagamentoFornitoreEntity.setImporto(new BigDecimal(accontoAcquisto.getImporto()));
        pagamentoFornitoreEntity.setMezzopagamento(Utils.getMezzo(accontoAcquisto.getTipo()));
        pagamentoFornitoreEntity.setNumerodocumento(documentoAcquistoEntity.getNumero());
        pagamentoFornitoreEntity.setDatadocumento(documentoAcquistoEntity.getData());
        pagamentoFornitoreEntity.setTipodocumento(documentoAcquistoEntity.getTipo());
        return pagamentoFornitoreEntity;
    };

    public void setFatturaAEntityDao(FatturaAEntityDao fatturaAEntityDao) {
        this.fatturaAEntityDao = fatturaAEntityDao;
    }

    public void setAcquistoDao(AcquistoDao acquistoDao) {
        this.acquistoDao = acquistoDao;
    }

    public void setFornitoreEntityDao(FornitoreEntityDao fornitoreEntityDao) {
        this.fornitoreEntityDao = fornitoreEntityDao;
    }

    public void setDescrizioniAcquistoDao(DescrizioniAcquistoDao descrizioniAcquistoDao) {
        this.descrizioniAcquistoDao = descrizioniAcquistoDao;
    }

    public void setDettaglioFatturaAEntityDao(DettaglioFatturaAEntityDao dettaglioFatturaAEntityDao) {
        this.dettaglioFatturaAEntityDao = dettaglioFatturaAEntityDao;
    }

    public void setProdottiMDao(ProdottiMDao prodottiMDao) {
        this.prodottiMDao = prodottiMDao;
    }

    public void setDdtFattureDao(DdtFattureDao ddtFattureDao) {
        this.ddtFattureDao = ddtFattureDao;
    }

    public void setTotaleFatturaAEntityDao(TotaleFatturaAEntityDao totaleFatturaAEntityDao) {
        this.totaleFatturaAEntityDao = totaleFatturaAEntityDao;
    }

    public void setDocumentoAcquistoEntityDao(DocumentoAcquistoEntityDao documentoAcquistoEntityDao) {
        this.documentoAcquistoEntityDao = documentoAcquistoEntityDao;
    }

    public void setAccontoAcquistoDao(AccontoAcquistoDao accontoAcquistoDao) {
        this.accontoAcquistoDao = accontoAcquistoDao;
    }

    public void setPagamentoFornitoreEntityDao(PagamentoFornitoreEntityDao pagamentoFornitoreEntityDao) {
        this.pagamentoFornitoreEntityDao = pagamentoFornitoreEntityDao;
    }

    public void setFornitoriCodiciDao(FornitoriCodiciDao fornitoriCodiciDao) {
        this.fornitoriCodiciDao = fornitoriCodiciDao;
    }
}
