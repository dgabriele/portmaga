package it.dommy.portmaga.service.impl;

import it.dommy.portmaga.daos.access.FornitoriDao;
import it.dommy.portmaga.daos.mysql.FornitoreEntityDao;
import it.dommy.portmaga.model.access.Fornitori;
import it.dommy.portmaga.model.mysql.FornitoreEntity;
import it.dommy.portmaga.service.FornitoriService;
import it.dommy.portmaga.util.Utils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by dommy on 10/24/15.
 */
@Service("fornitoriService")
public class FornitoriServiceImpl implements FornitoriService{
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(FornitoriServiceImpl.class.getName());

    FornitoriDao fornitoriDao;
    FornitoreEntityDao fornitoreEntityDao;

    @Override
    public void copyFornitori(){
        LOGGER.debug("copyFornitori start");
        List<Fornitori> listFornitory=fornitoriDao.findAll();
        List<FornitoreEntity> listFornitoreEntities=listFornitory.stream().map(copyfornitoryFunction).collect(Collectors.toList());
        listFornitoreEntities.stream().forEach(fornitoreEntity -> fornitoreEntityDao.save(fornitoreEntity));
        LOGGER.debug("copyFornitori start");
    }

    Function<Fornitori, FornitoreEntity>copyfornitoryFunction = fornitori ->{
        FornitoreEntity fornitoreEntity=new FornitoreEntity();
        fornitoreEntity.setCodiceFornitoreInterno(fornitori.getCodiceFornitore());
        fornitoreEntity.setRagionesociale(Utils.replaceApiceClienti(Utils.trimInitEndSpace(fornitori.getRagioneSociale())));
        fornitoreEntity.setCitta(Utils.trimInitEndSpace(Utils.replaceApiceClienti(Utils.extractCity(fornitori.getCitta()))));
        fornitoreEntity.setCap(Utils.trimInitEndSpace(Utils.extractCap(fornitori.getCitta())));
        fornitoreEntity.setProvincia(Utils.trimInitEndSpace(Utils.replaceApiceClienti(Utils.extractProvince(fornitori.getCitta()))));
        fornitoreEntity.setVia(Utils.trimInitEndSpace(Utils.replaceApiceClienti(fornitori.getIndirizzo())));
        fornitoreEntity.setCf(Utils.trimInitEndSpace(fornitori.getCodiceFiscale()));
        fornitoreEntity.setPi(Utils.trimInitEndSpace(fornitori.getPartitaIva()));
        fornitoreEntity.setTelefono1(Utils.trimInitEndSpace(Utils.subStringTelefono(fornitori.getTelefono())));

        return  fornitoreEntity;
    } ;

    public void setFornitoriDao(FornitoriDao fornitoriDao) {
        this.fornitoriDao = fornitoriDao;
    }

    public void setFornitoreEntityDao(FornitoreEntityDao fornitoreEntityDao) {
        this.fornitoreEntityDao = fornitoreEntityDao;
    }
}
