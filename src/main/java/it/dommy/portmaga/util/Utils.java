package it.dommy.portmaga.util;

import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dommy on 8/9/15.
 */
public class Utils {
    public static final BigDecimal ONEHUNDERT = new BigDecimal(100);
    public static BigDecimal percentage(BigDecimal base, BigDecimal pct){
        return base.add(base.multiply(pct).divide(ONEHUNDERT));
    }

    public static String extractCap(String city){
        String newCity=city.replaceAll("[^\\d+]", "");
        return newCity.length()==5 ? newCity:"";
    }

    public static String extractCity(String city){
        if(city.contains("-"))
            return extractCityWithHyphen(city);
        else
            return  extractCityWithoutHyphen(city);
    }

    public static String extractProvince(String city){
        if(city.contains("-"))
            return extractProvinceWithHyphen(city);
        else
            return  extractProvinceWithoutHyphen(city);
    }

    public static String extractCityWithHyphen(String city){
        String cityProvince=city.replaceAll("[\\d+]", "");

        String cityProvinceA[]=cityProvince.split("-");
//      La citta sarà la prima stringa con lunghezza superiore a 2 altrimenti è provincia
        for(String test:cityProvinceA){
            if(test.length()>2)
                return test;
        }
        return "";
    }

    public static String extractCityWithoutHyphen(String city){
        String cityProvince=city.replaceAll("[\\d+]", "");
        if(cityProvince.toUpperCase().contains("STEFANO")){
            cityProvince=cityProvince.toUpperCase().replaceAll("CS","");
            return cityProvince;
        }
        else {
            String cityProvinceA[] = cityProvince.split(" ");
//      La citta sarà la prima stringa con lunghezza superiore a 2 altrimenti è provincia
            for (String test : cityProvinceA) {
                if (test.length() > 2)
                    return test;
            }
            return "";
        }
    }

    public static String extractProvinceWithHyphen(String city){
        String cityProvince=city.replaceAll("[\\d+]", "");
        cityProvince=cityProvince.replaceAll("\\s","");
        String cityProvinceA[]=cityProvince.split("-");
//      La citta sarà la prima stringa con lunghezza superiore a 2 altrimenti è provincia
        for(String test:cityProvinceA){
            if(test.length()==2)
                return test;
        }
        return "";
    }

    public static String extractProvinceWithoutHyphen(String city){
        String cityProvince=city.replaceAll("[\\d+]", "");
        if(cityProvince.toUpperCase().contains("STEFANO"))
            return "CS";
        else {
            String cityProvinceA[] = cityProvince.split(" ");
//      La citta sarà la prima stringa con lunghezza superiore a 2 altrimenti è provincia
            for (String test : cityProvinceA) {
                if (test.length() == 2)
                    return test;
            }
            return "";
        }
    }

    public static int getAliquotaByImponibileAndImposta(double imponibile,double imposta){
        return (int) Math.round((((imposta * 100) / imponibile)));
    }

    public static boolean isInteger( String input ) {
        try {
            Integer.parseInt( input );
            return true;
        }
        catch( NumberFormatException e ) {
            return false;
        }
    }

    public static short getMonth(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
//      +1 because Calendar is Zero Based
        return (short) (calendar.get(Calendar.MONTH)+1);
    }

    public static short getYear(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        return (short) (calendar.get(Calendar.YEAR));
    }

    public static void containApice(String tipo,String string){
        if(string.contains("'"))
            System.out.println(tipo+" "+string);
    }

    public static String replaceApiceProdotti(String string){
        if(string.contains("'"))
        {
            int index=string.indexOf("'");
            if(isNumeric(String.valueOf(string.charAt(index-1))))
                return string.replaceAll("'","°");
            else
                return string.replaceAll("'"," ");

        }
        return  string;
    }

    public static String replaceApiceClienti(String string){
        if(string.contains("'"))
                return string.replaceAll("'","’");

        return  string;
    }

    public static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    public static String replaceTrasporto(String trasporto){
        if(!trasporto.equalsIgnoreCase("mittente") && !trasporto.equalsIgnoreCase("destinatario")){
            return "destinatario";
        }
        return  trasporto;
    }

    public static String subStringTelefono(String telefono){
        if(telefono!=null && telefono.length()>15)
            return telefono.substring(0,15);
        return telefono;
    }

    public static String trimInitEndSpace(String string){
       string= StringUtils.stripStart(string,null);
        string= StringUtils.stripEnd(string,null);
        return string;
    }
    public static String replaceMisura(String misura){
        misura=trimInitEndSpace(misura);
        if (misura.equalsIgnoreCase("MT"))
            return "ML";
        else if(misura.equalsIgnoreCase("BA"))
            return "PZ";
        else if(misura.equalsIgnoreCase("CF"))
            return "PZ";
        else if(misura.equalsIgnoreCase("LA"))
            return "PZ";
        else if(misura.equalsIgnoreCase("CP"))
            return "PZ";
        else if(misura.equalsIgnoreCase("BI"))
            return "PZ";
        else if(misura.equalsIgnoreCase("FL"))
            return "PZ";
        else if(misura.equalsIgnoreCase("BL"))
            return "PZ";
        else if(misura.equalsIgnoreCase("NR"))
            return "PZ";
        else if(misura.equalsIgnoreCase("BO"))
            return "PZ";
        else if(misura.equalsIgnoreCase("UN"))
            return "PZ";
        else if(misura.equalsIgnoreCase("TA"))
            return "PZ";
        else if(misura.equalsIgnoreCase("CA"))
            return "PZ";
        return trimInitEndSpace(misura);
    }
    public static BigDecimal getRicarico(final BigDecimal prezzo,final BigDecimal listino){
       return ((listino.divide(prezzo,2, RoundingMode.HALF_UP)).multiply(new BigDecimal(100))).subtract(new BigDecimal(100));
    }

    public static String getMezzo(final String tipopag){
        final String contanti="CONTANTI";
        if(tipopag.contains("DETR"))
            return contanti;
        else if(tipopag.contains("OMA"))
            return contanti;
        else if(tipopag.contains("A RIC"))
            return contanti;
        else
            return trimInitEndSpace(tipopag);
    }
}
