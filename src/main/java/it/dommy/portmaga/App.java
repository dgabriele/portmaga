package it.dommy.portmaga;

import it.dommy.portmaga.model.mysql.FatturaAEntity;
import it.dommy.portmaga.service.*;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.sql.*;

/**
 * Hello world!
 *
 */
public class App 
{
    @Autowired
    private ProdottiService prodottiService;

    @Autowired
    private MagazzinoService magazzinoService;

    @Autowired
    private ClientiService clientiService;

    @Autowired
    private FattureService fattureService;

    @Autowired
    private FornitoriService fornitoriService;

    @Autowired
    private FatturaAService fatturaAService;

    private void creazioniBase(){
//      Creo prima il magazzino che è necessario hai prodotti
        magazzinoService.createMagazzino();
        prodottiService.copyProdotti();
        clientiService.copyClientiToClienteEntity();
        fattureService.copyFatture();
        fornitoriService.copyFornitori();
        fatturaAService.copyFattureAcquisto();
    }

    public static void main( String[] args )
    {
//                try {
//            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
//            Connection conn= DriverManager.getConnection("jdbc:ucanaccess://src/main/resources/Maga.mdb", "", "Compumaint");
//            Statement statement= conn.createStatement();
//            ResultSet set= statement.executeQuery("Select codice from PRODOTTI");
//            while(set.next()){
//                System.out.println(set.getString(1));
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }

        ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");
        BeanFactory contex = ctx;
        App app=new App();
        AutowireCapableBeanFactory acbf = ctx.getAutowireCapableBeanFactory();
        acbf.autowireBeanProperties(app, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
        acbf.initializeBean(app, "app"); // any name will work
        app.creazioniBase();

    }
}
