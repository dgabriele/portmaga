package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "dettaglio_fattura")
public class DettaglioFatturaEntity implements Model {
    private int iddettaglio;
    public FatturaEntity fatturaEntity;
    private ClienteEntity clienteEntity;
    private Magazzino magazzino;
    private ProdottoEntity prodottoEntity;
    private BigDecimal quantita;
    private String um;
    private String descrizione;
    private BigDecimal prezzo;
    private String sconto;
    private BigDecimal prezzos;
    private BigDecimal aliquota;
    private BigDecimal importo;
    private BigDecimal imposta;

    @Id
    @GeneratedValue
    @Column(name = "IDDETTAGLIO", nullable = false)
    public int getIddettaglio() {
        return iddettaglio;
    }

    public void setIddettaglio(int iddettaglio) {
        this.iddettaglio = iddettaglio;
    }

    @ManyToOne
    @JoinColumn(name = "IDFATTURA",referencedColumnName = "IDFATTURA")
    public FatturaEntity getFatturaEntity() {
        return fatturaEntity;
    }

    public void setFatturaEntity(FatturaEntity fatturaEntity) {
        this.fatturaEntity = fatturaEntity;
    }

    @ManyToOne
    @JoinColumn(name = "CODICECLIENTE",referencedColumnName ="CODICECLIENTE")
    public ClienteEntity getClienteEntity() {
        return clienteEntity;
    }

    public void setClienteEntity(ClienteEntity clienteEntity) {
        this.clienteEntity = clienteEntity;
    }

    @ManyToOne
    @JoinColumn(name = "CODICEMAGAZZINO",referencedColumnName = "CODICEMAGAZZINO")
    public Magazzino getMagazzino() {
        return magazzino;
    }

    public void setMagazzino(Magazzino magazzino) {
        this.magazzino = magazzino;
    }

    @ManyToOne
    @JoinColumn(name = "CODICEPRODOTTO",referencedColumnName = "CODICEPRODOTTO")
    public ProdottoEntity getProdottoEntity() {
        return prodottoEntity;
    }

    public void setProdottoEntity(ProdottoEntity prodottoEntity) {
        this.prodottoEntity = prodottoEntity;
    }

    private String codiceprodottointerno;

    @Basic
    @Column(name = "CODICEPRODOTTOINTERNO", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCodiceprodottointerno() {
        return codiceprodottointerno;
    }

    public void setCodiceprodottointerno(String codiceprodottointerno) {
        this.codiceprodottointerno = codiceprodottointerno;
    }

    @Basic
    @Column(name = "QUANTITA", nullable = true, insertable = true, updatable = true, precision = 4)
    public BigDecimal getQuantita() {
        return quantita;
    }

    public void setQuantita(BigDecimal quantita) {
        this.quantita = quantita;
    }

    @Basic
    @Column(name = "UM", nullable = true, insertable = true, updatable = true, length = 10)
    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    @Basic
    @Column(name = "DESCRIZIONE", nullable = true, insertable = true, updatable = true, length = 500)
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    @Basic
    @Column(name = "PREZZO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    @Basic
    @Column(name = "SCONTO", nullable = true, insertable = true, updatable = true, length = 40)
    public String getSconto() {
        return sconto;
    }

    public void setSconto(String sconto) {
        this.sconto = sconto;
    }

    @Basic
    @Column(name = "PREZZOS", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getPrezzos() {
        return prezzos;
    }

    public void setPrezzos(BigDecimal prezzos) {
        this.prezzos = prezzos;
    }

    @Basic
    @Column(name = "ALIQUOTA", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota = aliquota;
    }

    @Basic
    @Column(name = "IMPORTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImporto() {
        return importo;
    }

    public void setImporto(BigDecimal importo) {
        this.importo = importo;
    }

    @Basic
    @Column(name = "IMPOSTA", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getImposta() {
        return imposta;
    }

    public void setImposta(BigDecimal imposta) {
        this.imposta = imposta;
    }

}
