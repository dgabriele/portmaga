package it.dommy.portmaga.model.mysql;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by dommy on 9/29/15.
 */
public class FatturaAEntityPK implements Serializable {
    private int codicefornitore;

    @Column(name = "CODICEFORNITORE", nullable = false, insertable = true, updatable = true)
    @Id
    public int getCodicefornitore() {
        return codicefornitore;
    }

    public void setCodicefornitore(int codicefornitore) {
        this.codicefornitore = codicefornitore;
    }

    private String numero;

    @Column(name = "NUMERO", nullable = false, insertable = true, updatable = true, length = 30)
    @Id
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    private Date data;

    @Column(name = "DATA", nullable = false, insertable = true, updatable = true)
    @Id
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    private String tipo;

    @Column(name = "TIPO", nullable = false, insertable = true, updatable = true, length = 6)
    @Id
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FatturaAEntityPK that = (FatturaAEntityPK) o;

        if (codicefornitore != that.codicefornitore) return false;
        if (numero != null ? !numero.equals(that.numero) : that.numero != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (tipo != null ? !tipo.equals(that.tipo) : that.tipo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codicefornitore;
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        return result;
    }
}
