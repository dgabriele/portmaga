package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by dommy on 9/29/15.
 */
@Entity
@Table(name = "PAGAMENTO_FORNITORE")
public class PagamentoFornitoreEntity implements Model {
    private int idpagamento;
    private int iddocumento;
    private int codicefornitore;
    private Date data;
    private String movimento;
    private String banca;
    private String numeroconto;
    private String operazione;
    private String mezzopagamento;
    private String riferimento;
    private Date scadenza;
    private BigDecimal importo;
    private String pagamento;
    private Date datapagamento;
    private String numerodocumento;
    private Date datadocumento;
    private String tipodocumento;
    private String note;

    @Id
    @GeneratedValue
    @Column(name = "IDPAGAMENTO", nullable = false, insertable = true, updatable = true)
    public int getIdpagamento() {
        return idpagamento;
    }

    public void setIdpagamento(int idpagamento) {
        this.idpagamento = idpagamento;
    }

    @Column(name = "IDDOCUMENTO", nullable = false, insertable = true, updatable = true)
    public int getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(int iddocumento) {
        this.iddocumento = iddocumento;
    }

    @Column(name = "CODICEFORNITORE", nullable = false, insertable = true, updatable = true)
    public int getCodicefornitore() {
        return codicefornitore;
    }

    public void setCodicefornitore(int codicefornitore) {
        this.codicefornitore = codicefornitore;
    }

    @Basic
    @Column(name = "DATA", nullable = true, insertable = true, updatable = true)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Basic
    @Column(name = "MOVIMENTO", nullable = true, insertable = true, updatable = true, length = 20)
    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    @Basic
    @Column(name = "BANCA", nullable = true, insertable = true, updatable = true, length = 100)
    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }

    @Basic
    @Column(name = "NUMEROCONTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getNumeroconto() {
        return numeroconto;
    }

    public void setNumeroconto(String numeroconto) {
        this.numeroconto = numeroconto;
    }

    @Basic
    @Column(name = "OPERAZIONE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getOperazione() {
        return operazione;
    }

    public void setOperazione(String operazione) {
        this.operazione = operazione;
    }

    @Basic
    @Column(name = "MEZZOPAGAMENTO", nullable = true, insertable = true, updatable = true, length = 50)
    public String getMezzopagamento() {
        return mezzopagamento;
    }

    public void setMezzopagamento(String mezzopagamento) {
        this.mezzopagamento = mezzopagamento;
    }

    @Basic
    @Column(name = "RIFERIMENTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getRiferimento() {
        return riferimento;
    }

    public void setRiferimento(String riferimento) {
        this.riferimento = riferimento;
    }

    @Basic
    @Column(name = "SCADENZA", nullable = true, insertable = true, updatable = true)
    public Date getScadenza() {
        return scadenza;
    }

    public void setScadenza(Date scadenza) {
        this.scadenza = scadenza;
    }

    @Basic
    @Column(name = "IMPORTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImporto() {
        return importo;
    }

    public void setImporto(BigDecimal importo) {
        this.importo = importo;
    }

    @Basic
    @Column(name = "PAGAMENTO", nullable = true, insertable = true, updatable = true, length = 15)
    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    @Basic
    @Column(name = "DATAPAGAMENTO", nullable = true, insertable = true, updatable = true)
    public Date getDatapagamento() {
        return datapagamento;
    }

    public void setDatapagamento(Date datapagamento) {
        this.datapagamento = datapagamento;
    }

    @Basic
    @Column(name = "NUMERODOCUMENTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getNumerodocumento() {
        return numerodocumento;
    }

    public void setNumerodocumento(String numerodocumento) {
        this.numerodocumento = numerodocumento;
    }

    @Basic
    @Column(name = "DATADOCUMENTO", nullable = true, insertable = true, updatable = true)
    public Date getDatadocumento() {
        return datadocumento;
    }

    public void setDatadocumento(Date datadocumento) {
        this.datadocumento = datadocumento;
    }

    @Basic
    @Column(name = "TIPODOCUMENTO", nullable = true, insertable = true, updatable = true, length = 6)
    public String getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PagamentoFornitoreEntity that = (PagamentoFornitoreEntity) o;

        if (idpagamento != that.idpagamento) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (movimento != null ? !movimento.equals(that.movimento) : that.movimento != null) return false;
        if (banca != null ? !banca.equals(that.banca) : that.banca != null) return false;
        if (numeroconto != null ? !numeroconto.equals(that.numeroconto) : that.numeroconto != null) return false;
        if (operazione != null ? !operazione.equals(that.operazione) : that.operazione != null) return false;
        if (mezzopagamento != null ? !mezzopagamento.equals(that.mezzopagamento) : that.mezzopagamento != null)
            return false;
        if (riferimento != null ? !riferimento.equals(that.riferimento) : that.riferimento != null) return false;
        if (scadenza != null ? !scadenza.equals(that.scadenza) : that.scadenza != null) return false;
        if (importo != null ? !importo.equals(that.importo) : that.importo != null) return false;
        if (pagamento != null ? !pagamento.equals(that.pagamento) : that.pagamento != null) return false;
        if (datapagamento != null ? !datapagamento.equals(that.datapagamento) : that.datapagamento != null)
            return false;
        if (numerodocumento != null ? !numerodocumento.equals(that.numerodocumento) : that.numerodocumento != null)
            return false;
        if (datadocumento != null ? !datadocumento.equals(that.datadocumento) : that.datadocumento != null)
            return false;
        if (tipodocumento != null ? !tipodocumento.equals(that.tipodocumento) : that.tipodocumento != null)
            return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idpagamento;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (movimento != null ? movimento.hashCode() : 0);
        result = 31 * result + (banca != null ? banca.hashCode() : 0);
        result = 31 * result + (numeroconto != null ? numeroconto.hashCode() : 0);
        result = 31 * result + (operazione != null ? operazione.hashCode() : 0);
        result = 31 * result + (mezzopagamento != null ? mezzopagamento.hashCode() : 0);
        result = 31 * result + (riferimento != null ? riferimento.hashCode() : 0);
        result = 31 * result + (scadenza != null ? scadenza.hashCode() : 0);
        result = 31 * result + (importo != null ? importo.hashCode() : 0);
        result = 31 * result + (pagamento != null ? pagamento.hashCode() : 0);
        result = 31 * result + (datapagamento != null ? datapagamento.hashCode() : 0);
        result = 31 * result + (numerodocumento != null ? numerodocumento.hashCode() : 0);
        result = 31 * result + (datadocumento != null ? datadocumento.hashCode() : 0);
        result = 31 * result + (tipodocumento != null ? tipodocumento.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }
}
