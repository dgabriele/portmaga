package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by dommy on 9/29/15.
 */
@Entity
@Table(name = "DOCUMENTO_ACQUISTO")
public class DocumentoAcquistoEntity implements Model{
    private int iddocumento;

    @Id
    @GeneratedValue
    @Column(name = "IDDOCUMENTO", nullable = false, insertable = true, updatable = true)
    public int getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(int iddocumento) {
        this.iddocumento = iddocumento;
    }

    private int codicefornitore;

    @Column(name = "CODICEFORNITORE", nullable = false, insertable = true, updatable = true)
    public int getCodicefornitore() {
        return codicefornitore;
    }

    public void setCodicefornitore(int codicefornitore) {
        this.codicefornitore = codicefornitore;
    }

    private String numero;

    @Basic
    @Column(name = "NUMERO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    private Date data;

    @Basic
    @Column(name = "DATA", nullable = true, insertable = true, updatable = true)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    private Short mese;

    @Basic
    @Column(name = "MESE", nullable = true, insertable = true, updatable = true)
    public Short getMese() {
        return mese;
    }

    public void setMese(Short mese) {
        this.mese = mese;
    }

    private Short anno;

    @Basic
    @Column(name = "ANNO", nullable = true, insertable = true, updatable = true)
    public Short getAnno() {
        return anno;
    }

    public void setAnno(Short anno) {
        this.anno = anno;
    }

    private String tipo;

    @Basic
    @Column(name = "TIPO", nullable = true, insertable = true, updatable = true, length = 6)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    private String modalitapagamento;

    @Basic
    @Column(name = "MODALITAPAGAMENTO", nullable = true, insertable = true, updatable = true, length = 100)
    public String getModalitapagamento() {
        return modalitapagamento;
    }

    public void setModalitapagamento(String modalitapagamento) {
        this.modalitapagamento = modalitapagamento;
    }

    private BigDecimal imponibile;

    @Basic
    @Column(name = "IMPONIBILE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile() {
        return imponibile;
    }

    public void setImponibile(BigDecimal imponibile) {
        this.imponibile = imponibile;
    }

    private BigDecimal imposta;

    @Basic
    @Column(name = "IMPOSTA", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta() {
        return imposta;
    }

    public void setImposta(BigDecimal imposta) {
        this.imposta = imposta;
    }

    private BigDecimal esente;

    @Basic
    @Column(name = "ESENTE", nullable = false, insertable = true, updatable = true, precision = 2)
    public BigDecimal getEsente() {
        return esente;
    }

    public void setEsente(BigDecimal esente) {
        this.esente = esente;
    }

    private BigDecimal ritenuta;

    @Basic
    @Column(name = "RITENUTA", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getRitenuta() {
        return ritenuta;
    }

    public void setRitenuta(BigDecimal ritenuta) {
        this.ritenuta = ritenuta;
    }

    private BigDecimal netto;

    @Basic
    @Column(name = "NETTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getNetto() {
        return netto;
    }

    public void setNetto(BigDecimal netto) {
        this.netto = netto;
    }

    private BigDecimal totale;

    @Basic
    @Column(name = "TOTALE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getTotale() {
        return totale;
    }

    public void setTotale(BigDecimal totale) {
        this.totale = totale;
    }

    private BigDecimal acconto;

    @Basic
    @Column(name = "ACCONTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAcconto() {
        return acconto;
    }

    public void setAcconto(BigDecimal acconto) {
        this.acconto = acconto;
    }

    private BigDecimal saldo;

    @Basic
    @Column(name = "SALDO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    private BigDecimal importoscadenza;

    @Basic
    @Column(name = "IMPORTOSCADENZA", nullable = false, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImportoscadenza() {
        return importoscadenza;
    }

    public void setImportoscadenza(BigDecimal importoscadenza) {
        this.importoscadenza = importoscadenza;
    }

    private BigDecimal importoinsoluto;

    @Basic
    @Column(name = "IMPORTOINSOLUTO", nullable = false, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImportoinsoluto() {
        return importoinsoluto;
    }

    public void setImportoinsoluto(BigDecimal importoinsoluto) {
        this.importoinsoluto = importoinsoluto;
    }

    private String note;

    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    private Long codiceAcquisto;

    @Basic
    @Column(name = "CODICEACQUISTO", nullable = true, insertable = true, updatable = true)
    public Long getCodiceAcquisto() {
        return codiceAcquisto;
    }

    public void setCodiceAcquisto(Long codiceAcquisto) {
        this.codiceAcquisto = codiceAcquisto;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocumentoAcquistoEntity that = (DocumentoAcquistoEntity) o;

        if (iddocumento != that.iddocumento) return false;
        if (numero != null ? !numero.equals(that.numero) : that.numero != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (mese != null ? !mese.equals(that.mese) : that.mese != null) return false;
        if (anno != null ? !anno.equals(that.anno) : that.anno != null) return false;
        if (tipo != null ? !tipo.equals(that.tipo) : that.tipo != null) return false;
        if (modalitapagamento != null ? !modalitapagamento.equals(that.modalitapagamento) : that.modalitapagamento != null)
            return false;
        if (imponibile != null ? !imponibile.equals(that.imponibile) : that.imponibile != null) return false;
        if (imposta != null ? !imposta.equals(that.imposta) : that.imposta != null) return false;
        if (esente != null ? !esente.equals(that.esente) : that.esente != null) return false;
        if (ritenuta != null ? !ritenuta.equals(that.ritenuta) : that.ritenuta != null) return false;
        if (netto != null ? !netto.equals(that.netto) : that.netto != null) return false;
        if (totale != null ? !totale.equals(that.totale) : that.totale != null) return false;
        if (acconto != null ? !acconto.equals(that.acconto) : that.acconto != null) return false;
        if (saldo != null ? !saldo.equals(that.saldo) : that.saldo != null) return false;
        if (importoscadenza != null ? !importoscadenza.equals(that.importoscadenza) : that.importoscadenza != null)
            return false;
        if (importoinsoluto != null ? !importoinsoluto.equals(that.importoinsoluto) : that.importoinsoluto != null)
            return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = iddocumento;
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (mese != null ? mese.hashCode() : 0);
        result = 31 * result + (anno != null ? anno.hashCode() : 0);
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        result = 31 * result + (modalitapagamento != null ? modalitapagamento.hashCode() : 0);
        result = 31 * result + (imponibile != null ? imponibile.hashCode() : 0);
        result = 31 * result + (imposta != null ? imposta.hashCode() : 0);
        result = 31 * result + (esente != null ? esente.hashCode() : 0);
        result = 31 * result + (ritenuta != null ? ritenuta.hashCode() : 0);
        result = 31 * result + (netto != null ? netto.hashCode() : 0);
        result = 31 * result + (totale != null ? totale.hashCode() : 0);
        result = 31 * result + (acconto != null ? acconto.hashCode() : 0);
        result = 31 * result + (saldo != null ? saldo.hashCode() : 0);
        result = 31 * result + (importoscadenza != null ? importoscadenza.hashCode() : 0);
        result = 31 * result + (importoinsoluto != null ? importoinsoluto.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }
}
