package it.dommy.portmaga.model.mysql;


import it.dommy.portmaga.model.Model;

import javax.persistence.*;

import java.math.BigDecimal;

/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "cliente")
public class ClienteEntity implements Model {


    private int codicecliente;

    @Id
    @GeneratedValue
    @Column(name = "CODICECLIENTE", nullable = false)
    public int getCodicecliente() {
        return codicecliente;
    }

    public void setCodicecliente(int codicecliente) {
        this.codicecliente = codicecliente;
    }

    private String nome;

    @Basic
    @Column(name = "NOME", nullable = true, insertable = true, updatable = true, length = 50)
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    private String cognome;

    @Basic
    @Column(name = "COGNOME", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    private String ragionesociale;

    @Basic
    @Column(name = "RAGIONESOCIALE", nullable = true, insertable = true, updatable = true, length = 200)
    public String getRagionesociale() {
        return ragionesociale;
    }

    public void setRagionesociale(String ragionesociale) {
        this.ragionesociale = ragionesociale;
    }

    private String via;

    @Basic
    @Column(name = "VIA", nullable = true, insertable = true, updatable = true, length = 50)
    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    private String cap;

    @Basic
    @Column(name = "CAP", nullable = true, insertable = true, updatable = true, length = 5)
    public String getCap() {
        return cap;
    }

    public void setCap(String cap) {
        this.cap = cap;
    }

    private String citta;

    @Basic
    @Column(name = "CITTA", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    private String provincia;

    @Basic
    @Column(name = "PROVINCIA", nullable = true, insertable = true, updatable = true, length = 30)
    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    private String cf;

    @Basic
    @Column(name = "CF", nullable = true, insertable = true, updatable = true, length = 16)
    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    private String pi;

    @Basic
    @Column(name = "PI", nullable = true, insertable = true, updatable = true, length = 11)
    public String getPi() {
        return pi;
    }

    public void setPi(String pi) {
        this.pi = pi;
    }

    private String telefono1;

    @Basic
    @Column(name = "TELEFONO1", nullable = true, insertable = true, updatable = true, length = 15)
    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    private String telefono2;

    @Basic
    @Column(name = "TELEFONO2", nullable = true, insertable = true, updatable = true, length = 15)
    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    private String telefono3;

    @Basic
    @Column(name = "TELEFONO3", nullable = true, insertable = true, updatable = true, length = 15)
    public String getTelefono3() {
        return telefono3;
    }

    public void setTelefono3(String telefono3) {
        this.telefono3 = telefono3;
    }

    private String fax;

    @Basic
    @Column(name = "FAX", nullable = true, insertable = true, updatable = true, length = 15)
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    private String banca;

    @Basic
    @Column(name = "BANCA", nullable = true, insertable = true, updatable = true, length = 50)
    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }

    private String cin;

    @Basic
    @Column(name = "CIN", nullable = true, insertable = true, updatable = true, length = 1)
    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    private String abi;

    @Basic
    @Column(name = "ABI", nullable = true, insertable = true, updatable = true, length = 5)
    public String getAbi() {
        return abi;
    }

    public void setAbi(String abi) {
        this.abi = abi;
    }

    private String cab;

    @Basic
    @Column(name = "CAB", nullable = true, insertable = true, updatable = true, length = 5)
    public String getCab() {
        return cab;
    }

    public void setCab(String cab) {
        this.cab = cab;
    }

    private String numeroconto;

    @Basic
    @Column(name = "NUMEROCONTO", nullable = true, insertable = true, updatable = true, length = 12)
    public String getNumeroconto() {
        return numeroconto;
    }

    public void setNumeroconto(String numeroconto) {
        this.numeroconto = numeroconto;
    }

    private String iban;

    @Basic
    @Column(name = "IBAN", nullable = true, insertable = true, updatable = true, length = 27)
    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    private String email;

    @Basic
    @Column(name = "EMAIL", nullable = true, insertable = true, updatable = true, length = 40)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String http;

    @Basic
    @Column(name = "HTTP", nullable = true, insertable = true, updatable = true, length = 40)
    public String getHttp() {
        return http;
    }

    public void setHttp(String http) {
        this.http = http;
    }

    private BigDecimal fido;

    @Basic
    @Column(name = "FIDO", nullable = true, insertable = true, updatable = true, precision = 4,scale=2)
    public BigDecimal getFido() {
        return fido;
    }

    public void setFido(BigDecimal fido) {
        this.fido = fido;
    }

    private Integer codicepagamento;

    @Basic
    @Column(name = "CODICEPAGAMENTO", nullable = true, insertable = true, updatable = true)
    public Integer getCodicepagamento() {
        return codicepagamento;
    }

    public void setCodicepagamento(Integer codicepagamento) {
        this.codicepagamento = codicepagamento;
    }

    private String modalitapagamento;

    @Basic
    @Column(name = "MODALITAPAGAMENTO", nullable = true, insertable = true, updatable = true, length = 100)
    public String getModalitapagamento() {
        return modalitapagamento;
    }

    public void setModalitapagamento(String modalitapagamento) {
        this.modalitapagamento = modalitapagamento;
    }

    private String listino;

    @Basic
    @Column(name = "LISTINO", nullable = true, insertable = true, updatable = true, length = 20)
    public String getListino() {
        return listino;
    }

    public void setListino(String listino) {
        this.listino = listino;
    }

    private String note;

    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 150)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    private Integer codiceagente;

    @Basic
    @Column(name = "CODICEAGENTE", nullable = true, insertable = true, updatable = true)
    public Integer getCodiceagente() {
        return codiceagente;
    }

    public void setCodiceagente(Integer codiceagente) {
        this.codiceagente = codiceagente;
    }

    private String codiceClienteInterno;

    @Basic
    @Column(name = "CODICECLIENTEINTERNO", nullable = true, insertable = true, updatable = true,length = 30)
    public String getCodiceClienteInterno() {
        return codiceClienteInterno;
    }

    public void setCodiceClienteInterno(String codiceClienteInterno) {
        this.codiceClienteInterno = codiceClienteInterno;
    }

    private String sconto;

    @Basic
    @Column(name = "SCONTO", nullable = true, insertable = true, updatable = true, length = 40)
    public String getSconto() {
        return sconto;
    }

    public void setSconto(String sconto) {
        this.sconto= sconto;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClienteEntity that = (ClienteEntity) o;

        if (codicecliente != that.codicecliente) return false;
        if (nome != null ? !nome.equals(that.nome) : that.nome != null) return false;
        if (cognome != null ? !cognome.equals(that.cognome) : that.cognome != null) return false;
        if (ragionesociale != null ? !ragionesociale.equals(that.ragionesociale) : that.ragionesociale != null)
            return false;
        if (via != null ? !via.equals(that.via) : that.via != null) return false;
        if (cap != null ? !cap.equals(that.cap) : that.cap != null) return false;
        if (citta != null ? !citta.equals(that.citta) : that.citta != null) return false;
        if (provincia != null ? !provincia.equals(that.provincia) : that.provincia != null) return false;
        if (cf != null ? !cf.equals(that.cf) : that.cf != null) return false;
        if (pi != null ? !pi.equals(that.pi) : that.pi != null) return false;
        if (telefono1 != null ? !telefono1.equals(that.telefono1) : that.telefono1 != null) return false;
        if (telefono2 != null ? !telefono2.equals(that.telefono2) : that.telefono2 != null) return false;
        if (telefono3 != null ? !telefono3.equals(that.telefono3) : that.telefono3 != null) return false;
        if (fax != null ? !fax.equals(that.fax) : that.fax != null) return false;
        if (banca != null ? !banca.equals(that.banca) : that.banca != null) return false;
        if (cin != null ? !cin.equals(that.cin) : that.cin != null) return false;
        if (abi != null ? !abi.equals(that.abi) : that.abi != null) return false;
        if (cab != null ? !cab.equals(that.cab) : that.cab != null) return false;
        if (numeroconto != null ? !numeroconto.equals(that.numeroconto) : that.numeroconto != null) return false;
        if (iban != null ? !iban.equals(that.iban) : that.iban != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (http != null ? !http.equals(that.http) : that.http != null) return false;
        if (fido != null ? !fido.equals(that.fido) : that.fido != null) return false;
        if (codicepagamento != null ? !codicepagamento.equals(that.codicepagamento) : that.codicepagamento != null)
            return false;
        if (modalitapagamento != null ? !modalitapagamento.equals(that.modalitapagamento) : that.modalitapagamento != null)
            return false;
        if (listino != null ? !listino.equals(that.listino) : that.listino != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (codiceagente != null ? !codiceagente.equals(that.codiceagente) : that.codiceagente != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codicecliente;
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (cognome != null ? cognome.hashCode() : 0);
        result = 31 * result + (ragionesociale != null ? ragionesociale.hashCode() : 0);
        result = 31 * result + (via != null ? via.hashCode() : 0);
        result = 31 * result + (cap != null ? cap.hashCode() : 0);
        result = 31 * result + (citta != null ? citta.hashCode() : 0);
        result = 31 * result + (provincia != null ? provincia.hashCode() : 0);
        result = 31 * result + (cf != null ? cf.hashCode() : 0);
        result = 31 * result + (pi != null ? pi.hashCode() : 0);
        result = 31 * result + (telefono1 != null ? telefono1.hashCode() : 0);
        result = 31 * result + (telefono2 != null ? telefono2.hashCode() : 0);
        result = 31 * result + (telefono3 != null ? telefono3.hashCode() : 0);
        result = 31 * result + (fax != null ? fax.hashCode() : 0);
        result = 31 * result + (banca != null ? banca.hashCode() : 0);
        result = 31 * result + (cin != null ? cin.hashCode() : 0);
        result = 31 * result + (abi != null ? abi.hashCode() : 0);
        result = 31 * result + (cab != null ? cab.hashCode() : 0);
        result = 31 * result + (numeroconto != null ? numeroconto.hashCode() : 0);
        result = 31 * result + (iban != null ? iban.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (http != null ? http.hashCode() : 0);
        result = 31 * result + (fido != null ? fido.hashCode() : 0);
        result = 31 * result + (codicepagamento != null ? codicepagamento.hashCode() : 0);
        result = 31 * result + (modalitapagamento != null ? modalitapagamento.hashCode() : 0);
        result = 31 * result + (listino != null ? listino.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (codiceagente != null ? codiceagente.hashCode() : 0);
        return result;
    }
}
