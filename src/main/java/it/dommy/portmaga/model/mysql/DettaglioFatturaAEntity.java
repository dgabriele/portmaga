package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by dommy on 9/29/15.
 */
@Entity
@Table(name = "DETTAGLIO_FATTURA_A")
public class DettaglioFatturaAEntity  implements Model{
    private int iddettaglio;
    private int codicefornitore;
    private String numero;
    private Date data;
    private String tipo;
    private Integer codicemagazzino;
    private Integer codiceprodotto;
    private BigDecimal quantita;
    private String um;
    private String descrizione;
    private BigDecimal prezzo;
    private String sconto;
    private BigDecimal prezzos;
    private BigDecimal aliquota;
    private BigDecimal importo;
    private BigDecimal imposta;
    private String CodiceArticoloFornitore;


    @Id
    @GeneratedValue
    @Column(name = "IDDETTAGLIO", nullable = false, insertable = true, updatable = true)
    public int getIddettaglio() {
        return iddettaglio;
    }

    public void setIddettaglio(int iddettaglio) {
        this.iddettaglio = iddettaglio;
    }

    @Column(name = "CODICEFORNITORE", nullable = false, insertable = true, updatable = true)
    public int getCodicefornitore() {
        return codicefornitore;
    }

    public void setCodicefornitore(int codicefornitore) {
        this.codicefornitore = codicefornitore;
    }

    @Column(name = "NUMERO", nullable = false, insertable = true, updatable = true, length = 30)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Column(name = "DATA", nullable = false, insertable = true, updatable = true)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Column(name = "TIPO", nullable = false, insertable = true, updatable = true, length = 6)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "CODICEMAGAZZINO", nullable = true, insertable = true, updatable = true)
    public Integer getCodicemagazzino() {
        return codicemagazzino;
    }

    public void setCodicemagazzino(Integer codicemagazzino) {
        this.codicemagazzino = codicemagazzino;
    }

    @Basic
    @Column(name = "CODICEPRODOTTO", nullable = true, insertable = true, updatable = true)
    public Integer getCodiceprodotto() {
        return codiceprodotto;
    }

    public void setCodiceprodotto(Integer codiceprodotto) {
        this.codiceprodotto = codiceprodotto;
    }

    private String codiceprodottointerno;

    @Basic
    @Column(name = "CODICEPRODOTTOINTERNO", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCodiceprodottointerno() {
        return codiceprodottointerno;
    }

    public void setCodiceprodottointerno(String codiceprodottointerno) {
        this.codiceprodottointerno = codiceprodottointerno;
    }

    @Basic
    @Column(name = "QUANTITA", nullable = true, insertable = true, updatable = true, precision = 8,scale = 2)
    public BigDecimal getQuantita() {
        return quantita;
    }

    public void setQuantita(BigDecimal quantita) {
        this.quantita = quantita;
    }

    @Basic
    @Column(name = "UM", nullable = true, insertable = true, updatable = true, length = 10)
    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    @Basic
    @Column(name = "DESCRIZIONE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    @Basic
    @Column(name = "PREZZO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    @Basic
    @Column(name = "SCONTO", nullable = true, insertable = true, updatable = true, length = 40)
    public String getSconto() {
        return sconto;
    }

    public void setSconto(String sconto) {
        this.sconto = sconto;
    }

    @Basic
    @Column(name = "PREZZOS", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getPrezzos() {
        return prezzos;
    }

    public void setPrezzos(BigDecimal prezzos) {
        this.prezzos = prezzos;
    }

    @Basic
    @Column(name = "ALIQUOTA", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota = aliquota;
    }

    @Basic
    @Column(name = "IMPORTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImporto() {
        return importo;
    }

    public void setImporto(BigDecimal importo) {
        this.importo = importo;
    }

    @Basic
    @Column(name = "IMPOSTA", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta() {
        return imposta;
    }

    public void setImposta(BigDecimal imposta) {
        this.imposta = imposta;
    }

    @Basic
    @Column(name = "CODICEARTICOLOFORNITORE", nullable = true, insertable = true, updatable = true)
    public String getCodiceArticoloFornitore() {
        return CodiceArticoloFornitore;
    }

    public void setCodiceArticoloFornitore(String codiceArticoloFornitore) {
        CodiceArticoloFornitore = codiceArticoloFornitore;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DettaglioFatturaAEntity that = (DettaglioFatturaAEntity) o;

        if (iddettaglio != that.iddettaglio) return false;
        if (codicemagazzino != null ? !codicemagazzino.equals(that.codicemagazzino) : that.codicemagazzino != null)
            return false;
        if (codiceprodotto != null ? !codiceprodotto.equals(that.codiceprodotto) : that.codiceprodotto != null)
            return false;
        if (quantita != null ? !quantita.equals(that.quantita) : that.quantita != null) return false;
        if (um != null ? !um.equals(that.um) : that.um != null) return false;
        if (descrizione != null ? !descrizione.equals(that.descrizione) : that.descrizione != null) return false;
        if (prezzo != null ? !prezzo.equals(that.prezzo) : that.prezzo != null) return false;
        if (sconto != null ? !sconto.equals(that.sconto) : that.sconto != null) return false;
        if (prezzos != null ? !prezzos.equals(that.prezzos) : that.prezzos != null) return false;
        if (aliquota != null ? !aliquota.equals(that.aliquota) : that.aliquota != null) return false;
        if (importo != null ? !importo.equals(that.importo) : that.importo != null) return false;
        if (imposta != null ? !imposta.equals(that.imposta) : that.imposta != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = iddettaglio;
        result = 31 * result + (codicemagazzino != null ? codicemagazzino.hashCode() : 0);
        result = 31 * result + (codiceprodotto != null ? codiceprodotto.hashCode() : 0);
        result = 31 * result + (quantita != null ? quantita.hashCode() : 0);
        result = 31 * result + (um != null ? um.hashCode() : 0);
        result = 31 * result + (descrizione != null ? descrizione.hashCode() : 0);
        result = 31 * result + (prezzo != null ? prezzo.hashCode() : 0);
        result = 31 * result + (sconto != null ? sconto.hashCode() : 0);
        result = 31 * result + (prezzos != null ? prezzos.hashCode() : 0);
        result = 31 * result + (aliquota != null ? aliquota.hashCode() : 0);
        result = 31 * result + (importo != null ? importo.hashCode() : 0);
        result = 31 * result + (imposta != null ? imposta.hashCode() : 0);
        return result;
    }
}
