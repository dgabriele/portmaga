package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;


/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "prodotto")
public class ProdottoEntity implements Model {

    private int codiceprodotto;

    @Id
    @GeneratedValue
    @Column(name = "CODICEPRODOTTO")
    public int getCodiceprodotto() {
        return codiceprodotto;
    }

    public void setCodiceprodotto(int codiceprodotto) {
        this.codiceprodotto = codiceprodotto;
    }

    private String codiceean;

    @Basic
    @Column(name = "CODICEEAN", nullable = true, insertable = true, updatable = true, length = 13)
    public String getCodiceean() {
        return codiceean;
    }

    public void setCodiceean(String codiceean) {
        this.codiceean = codiceean;
    }

    private Integer codicemerceologico;

    @Basic
    @Column(name = "CODICEMERCEOLOGICO", nullable = true, insertable = true, updatable = true)
    public Integer getCodicemerceologico() {
        return codicemerceologico;
    }

    public void setCodicemerceologico(Integer codicemerceologico) {
        this.codicemerceologico = codicemerceologico;
    }

    private String codiceprodottofornitore;

    @Basic
    @Column(name = "CODICEPRODOTTOFORNITORE", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCodiceprodottofornitore() {
        return codiceprodottofornitore;
    }

    public void setCodiceprodottofornitore(String codiceprodottofornitore) {
        this.codiceprodottofornitore = codiceprodottofornitore;
    }

    private String codiceprodottointerno;

    @Basic
    @Column(name = "CODICEPRODOTTOINTERNO", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCodiceprodottointerno() {
        return codiceprodottointerno;
    }

    public void setCodiceprodottointerno(String codiceprodottointerno) {
        this.codiceprodottointerno = codiceprodottointerno;
    }

    private String descrizione;

    @Basic
    @Column(name = "DESCRIZIONE", nullable = true, insertable = true, updatable = true, length = 200)
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    private String umacquisto;

    @Basic
    @Column(name = "UMACQUISTO", nullable = true, insertable = true, updatable = true, length = 10)
    public String getUmacquisto() {
        return umacquisto;
    }

    public void setUmacquisto(String umacquisto) {
        this.umacquisto = umacquisto;
    }

    private BigDecimal fattoreconversione;

    @Basic
    @Column(name = "FATTORECONVERSIONE", nullable = true, insertable = true, updatable = true, precision = 4,scale=2)
    public BigDecimal getFattoreconversione() {
        return fattoreconversione;
    }

    public void setFattoreconversione(BigDecimal fattoreconversione) {
        this.fattoreconversione = fattoreconversione;
    }

    private String umvendita;

    @Basic
    @Column(name = "UMVENDITA", nullable = true, insertable = true, updatable = true, length = 10)
    public String getUmvendita() {
        return umvendita;
    }

    public void setUmvendita(String umvendita) {
        this.umvendita = umvendita;
    }

    private String conversione;

    @Basic
    @Column(name = "CONVERSIONE", nullable = true, insertable = true, updatable = true, length = 50)
    public String getConversione() {
        return conversione;
    }

    public void setConversione(String conversione) {
        this.conversione = conversione;
    }

    private BigDecimal confezione;

    @Basic
    @Column(name = "CONFEZIONE", nullable = true, insertable = true, updatable = true, precision = 4,scale=2)
    public BigDecimal getConfezione() {
        return confezione;
    }

    public void setConfezione(BigDecimal confezione) {
        this.confezione = confezione;
    }

    private String classificazione;

    @Basic
    @Column(name = "CLASSIFICAZIONE", nullable = true, insertable = true, updatable = true, length = 50)
    public String getClassificazione() {
        return classificazione;
    }

    public void setClassificazione(String classificazione) {
        this.classificazione = classificazione;
    }

    private String tipologia;

    @Basic
    @Column(name = "TIPOLOGIA", nullable = true, insertable = true, updatable = true, length = 50)
    public String getTipologia() {
        return tipologia;
    }

    public void setTipologia(String tipologia) {
        this.tipologia = tipologia;
    }

    private String colore;

    @Basic
    @Column(name = "COLORE", nullable = true, insertable = true, updatable = true, length = 50)
    public String getColore() {
        return colore;
    }

    public void setColore(String colore) {
        this.colore = colore;
    }

    private BigDecimal aliquota;

    @Basic
    @Column(name = "ALIQUOTA", nullable = true, insertable = true, updatable = true, precision = 4,scale=2)
    public BigDecimal getAliquota() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota = aliquota;
    }

    private BigDecimal prezzoacquisto;

    @Basic
    @Column(name = "PREZZOACQUISTO", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getPrezzoacquisto() {
        return prezzoacquisto;
    }

    public void setPrezzoacquisto(BigDecimal prezzoacquisto) {
        this.prezzoacquisto = prezzoacquisto;
    }

    private BigDecimal prezzoacquistoconvertito;

    @Basic
    @Column(name = "PREZZOACQUISTOCONVERTITO", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getPrezzoacquistoconvertito() {
        return prezzoacquistoconvertito;
    }

    public void setPrezzoacquistoconvertito(BigDecimal prezzoacquistoconvertito) {
        this.prezzoacquistoconvertito = prezzoacquistoconvertito;
    }

    private BigDecimal prezzo;

    @Basic
    @Column(name = "PREZZO", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    private BigDecimal prezzoiva;

    @Basic
    @Column(name = "PREZZOIVA", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getPrezzoiva() {
        return prezzoiva;
    }

    public void setPrezzoiva(BigDecimal prezzoiva) {
        this.prezzoiva = prezzoiva;
    }

    private BigDecimal ricarico1;

    @Basic
    @Column(name = "RICARICO1", nullable = true, insertable = true, updatable = true, precision = 15,scale=2)
    public BigDecimal getRicarico1() {
        return ricarico1;
    }

    public void setRicarico1(BigDecimal ricarico1) {
        this.ricarico1 = ricarico1;
    }

    private BigDecimal listino1;

    @Basic
    @Column(name = "LISTINO1", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino1() {
        return listino1;
    }

    public void setListino1(BigDecimal listino1) {
        this.listino1 = listino1;
    }

    private BigDecimal listino1Iva;

    @Basic
    @Column(name = "LISTINO1IVA", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino1Iva() {
        return listino1Iva;
    }

    public void setListino1Iva(BigDecimal listino1Iva) {
        this.listino1Iva = listino1Iva;
    }

    private BigDecimal ricarico2;

    @Basic
    @Column(name = "RICARICO2", nullable = true, insertable = true, updatable = true, precision = 15,scale=2)
    public BigDecimal getRicarico2() {
        return ricarico2;
    }

    public void setRicarico2(BigDecimal ricarico2) {
        this.ricarico2 = ricarico2;
    }

    private BigDecimal listino2;

    @Basic
    @Column(name = "LISTINO2", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino2() {
        return listino2;
    }

    public void setListino2(BigDecimal listino2) {
        this.listino2 = listino2;
    }

    private BigDecimal listino2Iva;

    @Basic
    @Column(name = "LISTINO2IVA", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino2Iva() {
        return listino2Iva;
    }

    public void setListino2Iva(BigDecimal listino2Iva) {
        this.listino2Iva = listino2Iva;
    }

    private BigDecimal ricarico3;

    @Basic
    @Column(name = "RICARICO3", nullable = true, insertable = true, updatable = true, precision = 4,scale=2)
    public BigDecimal getRicarico3() {
        return ricarico3;
    }

    public void setRicarico3(BigDecimal ricarico3) {
        this.ricarico3 = ricarico3;
    }

    private BigDecimal listino3;

    @Basic
    @Column(name = "LISTINO3", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino3() {
        return listino3;
    }

    public void setListino3(BigDecimal listino3) {
        this.listino3 = listino3;
    }

    private BigDecimal listino3Iva;

    @Basic
    @Column(name = "LISTINO3IVA", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino3Iva() {
        return listino3Iva;
    }

    public void setListino3Iva(BigDecimal listino3Iva) {
        this.listino3Iva = listino3Iva;
    }

    private BigDecimal ricarico4;

    @Basic
    @Column(name = "RICARICO4", nullable = true, insertable = true, updatable = true, precision = 4,scale=2)
    public BigDecimal getRicarico4() {
        return ricarico4;
    }

    public void setRicarico4(BigDecimal ricarico4) {
        this.ricarico4 = ricarico4;
    }

    private BigDecimal listino4;

    @Basic
    @Column(name = "LISTINO4", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino4() {
        return listino4;
    }

    public void setListino4(BigDecimal listino4) {
        this.listino4 = listino4;
    }

    private BigDecimal listino4Iva;

    @Basic
    @Column(name = "LISTINO4IVA", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getListino4Iva() {
        return listino4Iva;
    }

    public void setListino4Iva(BigDecimal listino4Iva) {
        this.listino4Iva = listino4Iva;
    }

    private BigDecimal scortasicurezza;

    @Basic
    @Column(name = "SCORTASICUREZZA", nullable = true, insertable = true, updatable = true, precision = 8,scale=2)
    public BigDecimal getScortasicurezza() {
        return scortasicurezza;
    }

    public void setScortasicurezza(BigDecimal scortasicurezza) {
        this.scortasicurezza = scortasicurezza;
    }

    private BigDecimal esistenza;

    @Basic
    @Column(name = "ESISTENZA", nullable = true, insertable = true, updatable = true, precision = 8,scale=2)
    public BigDecimal getEsistenza() {
        return esistenza;
    }

    public void setEsistenza(BigDecimal esistenza) {
        this.esistenza = esistenza;
    }

    private BigDecimal costomedioponderato;

    @Basic
    @Column(name = "COSTOMEDIOPONDERATO", nullable = true, insertable = true, updatable = true, precision = 6,scale=2)
    public BigDecimal getCostomedioponderato() {
        return costomedioponderato;
    }

    public void setCostomedioponderato(BigDecimal costomedioponderato) {
        this.costomedioponderato = costomedioponderato;
    }

    private String ubicazione;

    @Basic
    @Column(name = "UBICAZIONE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getUbicazione() {
        return ubicazione;
    }

    public void setUbicazione(String ubicazione) {
        this.ubicazione = ubicazione;
    }


    private Magazzino magazzino;

    @OneToOne
    @JoinColumn(name = "CODICEMAGAZZINO")
    public Magazzino getMagazzino() {
        return magazzino;
    }

    public void setMagazzino(Magazzino magazzino) {
        this.magazzino = magazzino;
    }

    private String note;

    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
