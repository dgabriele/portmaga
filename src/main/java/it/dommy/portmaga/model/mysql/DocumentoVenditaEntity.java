package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "documento_vendita")
public class DocumentoVenditaEntity implements Model {

    private int iddocumento;

    private ClienteEntity clienteEntity;
    @Id
    @GeneratedValue
    @Column(name = "IDDOCUMENTO", nullable = false, insertable = true, updatable = true)
    public int getIddocumento() {
        return iddocumento;
    }

    public void setIddocumento(int iddocumento) {
        this.iddocumento = iddocumento;
    }

    @ManyToOne
    @JoinColumn(name = "CODICECLIENTE",referencedColumnName = "CODICECLIENTE")
    public ClienteEntity getClienteEntity() {
        return clienteEntity;
    }

    public void setClienteEntity(ClienteEntity clienteEntity) {
        this.clienteEntity = clienteEntity;
    }

    private Integer codiceagente;

    @Basic
    @Column(name = "CODICEAGENTE", nullable = true, insertable = true, updatable = true)
    public Integer getCodiceagente() {
        return codiceagente;
    }

    public void setCodiceagente(Integer codiceagente) {
        this.codiceagente = codiceagente;
    }

    private String numero;

    @Basic
    @Column(name = "NUMERO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    private Date data;

    @Basic
    @Column(name = "DATA", nullable = true, insertable = true, updatable = true)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    private Short mese;

    @Basic
    @Column(name = "MESE", nullable = true, insertable = true, updatable = true)
    public Short getMese() {
        return mese;
    }

    public void setMese(Short mese) {
        this.mese = mese;
    }

    private Short anno;

    @Basic
    @Column(name = "ANNO", nullable = true, insertable = true, updatable = true)
    public Short getAnno() {
        return anno;
    }

    public void setAnno(Short anno) {
        this.anno = anno;
    }

    private String tipo;

    @Basic
    @Column(name = "TIPO", nullable = true, insertable = true, updatable = true, length = 6)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    private String identificativodocumento;

    @Basic
    @Column(name = "IDENTIFICATIVODOCUMENTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getIdentificativodocumento() {
        return identificativodocumento;
    }

    public void setIdentificativodocumento(String identificativodocumento) {
        this.identificativodocumento = identificativodocumento;
    }

    private String modalitapagamento;

    @Basic
    @Column(name = "MODALITAPAGAMENTO", nullable = true, insertable = true, updatable = true, length = 100)
    public String getModalitapagamento() {
        return modalitapagamento;
    }

    public void setModalitapagamento(String modalitapagamento) {
        this.modalitapagamento = modalitapagamento;
    }

    private BigDecimal imponibile;

    @Basic
    @Column(name = "IMPONIBILE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile() {
        return imponibile;
    }

    public void setImponibile(BigDecimal imponibile) {
        this.imponibile = imponibile;
    }

    private BigDecimal imposta;

    @Basic
    @Column(name = "IMPOSTA", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta() {
        return imposta;
    }

    public void setImposta(BigDecimal imposta) {
        this.imposta = imposta;
    }

    private BigDecimal esente;

    @Basic
    @Column(name = "ESENTE", nullable = false, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getEsente() {
        return esente;
    }

    public void setEsente(BigDecimal esente) {
        this.esente = esente;
    }

    private BigDecimal totale;

    @Basic
    @Column(name = "TOTALE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getTotale() {
        return totale;
    }

    public void setTotale(BigDecimal totale) {
        this.totale = totale;
    }

    private BigDecimal acconto;

    @Basic
    @Column(name = "ACCONTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAcconto() {
        return acconto;
    }

    public void setAcconto(BigDecimal acconto) {
        this.acconto = acconto;
    }

    private BigDecimal saldo;

    @Basic
    @Column(name = "SALDO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    private BigDecimal importoscadenza;

    @Basic
    @Column(name = "IMPORTOSCADENZA", nullable = false, insertable = true, updatable = true, precision = 2)
    public BigDecimal getImportoscadenza() {
        return importoscadenza;
    }

    public void setImportoscadenza(BigDecimal importoscadenza) {
        this.importoscadenza = importoscadenza;
    }

    private BigDecimal importoinsoluto;

    @Basic
    @Column(name = "IMPORTOINSOLUTO", nullable = false, insertable = true, updatable = true, precision = 2)
    public BigDecimal getImportoinsoluto() {
        return importoinsoluto;
    }

    public void setImportoinsoluto(BigDecimal importoinsoluto) {
        this.importoinsoluto = importoinsoluto;
    }

    private BigDecimal provvigione;

    @Basic
    @Column(name = "PROVVIGIONE", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getProvvigione() {
        return provvigione;
    }

    public void setProvvigione(BigDecimal provvigione) {
        this.provvigione = provvigione;
    }

    private BigDecimal imponibileprovvigione;

    @Basic
    @Column(name = "IMPONIBILEPROVVIGIONE", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getImponibileprovvigione() {
        return imponibileprovvigione;
    }

    public void setImponibileprovvigione(BigDecimal imponibileprovvigione) {
        this.imponibileprovvigione = imponibileprovvigione;
    }

    private String riferimento;

    @Basic
    @Column(name = "RIFERIMENTO", nullable = true, insertable = true, updatable = true, length = 100)
    public String getRiferimento() {
        return riferimento;
    }

    public void setRiferimento(String riferimento) {
        this.riferimento = riferimento;
    }

    private String note;

    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    private Integer numeroprogressivo;

    @Basic
    @Column(name = "NUMEROPROGRESSIVO", nullable = true, insertable = true, updatable = true)
    public Integer getNumeroprogressivo() {
        return numeroprogressivo;
    }

    public void setNumeroprogressivo(Integer numeroprogressivo) {
        this.numeroprogressivo = numeroprogressivo;
    }


}
