package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by dommy on 9/29/15.
 */
@Entity
@Table(name = "TOTALE_FATTURA_A")
public class TotaleFatturaAEntity implements Model {
    private int idtotale;
    private BigDecimal imponibile1;
    private BigDecimal aliquota1;
    private BigDecimal imposta1;
    private BigDecimal imponibile2;
    private BigDecimal aliquota2;
    private BigDecimal imposta2;
    private BigDecimal imponibile3;
    private BigDecimal aliquota3;
    private BigDecimal imposta3;
    private BigDecimal imponibile;
    private BigDecimal scperc;
    private BigDecimal sconto;
    private BigDecimal imponibiles;
    private BigDecimal imposta;
    private BigDecimal esente;
    private BigDecimal totale;
    private FatturaAEntity fatturaA;

    @Id
    @GeneratedValue
    @Column(name = "IDTOTALE", nullable = false, insertable = true, updatable = true)
    public int getIdtotale() {
        return idtotale;
    }

    public void setIdtotale(int idtotale) {
        this.idtotale = idtotale;
    }

    @Basic
    @Column(name = "IMPONIBILE1", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile1() {
        return imponibile1;
    }

    public void setImponibile1(BigDecimal imponibile1) {
        this.imponibile1 = imponibile1;
    }

    @Basic
    @Column(name = "ALIQUOTA1", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota1() {
        return aliquota1;
    }

    public void setAliquota1(BigDecimal aliquota1) {
        this.aliquota1 = aliquota1;
    }

    @Basic
    @Column(name = "IMPOSTA1", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta1() {
        return imposta1;
    }

    public void setImposta1(BigDecimal imposta1) {
        this.imposta1 = imposta1;
    }

    @Basic
    @Column(name = "IMPONIBILE2", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile2() {
        return imponibile2;
    }

    public void setImponibile2(BigDecimal imponibile2) {
        this.imponibile2 = imponibile2;
    }

    @Basic
    @Column(name = "ALIQUOTA2", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota2() {
        return aliquota2;
    }

    public void setAliquota2(BigDecimal aliquota2) {
        this.aliquota2 = aliquota2;
    }

    @Basic
    @Column(name = "IMPOSTA2", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta2() {
        return imposta2;
    }

    public void setImposta2(BigDecimal imposta2) {
        this.imposta2 = imposta2;
    }

    @Basic
    @Column(name = "IMPONIBILE3", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile3() {
        return imponibile3;
    }

    public void setImponibile3(BigDecimal imponibile3) {
        this.imponibile3 = imponibile3;
    }

    @Basic
    @Column(name = "ALIQUOTA3", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota3() {
        return aliquota3;
    }

    public void setAliquota3(BigDecimal aliquota3) {
        this.aliquota3 = aliquota3;
    }

    @Basic
    @Column(name = "IMPOSTA3", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta3() {
        return imposta3;
    }

    public void setImposta3(BigDecimal imposta3) {
        this.imposta3 = imposta3;
    }

    @Basic
    @Column(name = "IMPONIBILE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile() {
        return imponibile;
    }

    public void setImponibile(BigDecimal imponibile) {
        this.imponibile = imponibile;
    }

    @Basic
    @Column(name = "SCPERC", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getScperc() {
        return scperc;
    }

    public void setScperc(BigDecimal scperc) {
        this.scperc = scperc;
    }

    @Basic
    @Column(name = "SCONTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getSconto() {
        return sconto;
    }

    public void setSconto(BigDecimal sconto) {
        this.sconto = sconto;
    }

    @Basic
    @Column(name = "IMPONIBILES", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibiles() {
        return imponibiles;
    }

    public void setImponibiles(BigDecimal imponibiles) {
        this.imponibiles = imponibiles;
    }

    @Basic
    @Column(name = "IMPOSTA", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta() {
        return imposta;
    }

    public void setImposta(BigDecimal imposta) {
        this.imposta = imposta;
    }

    @Basic
    @Column(name = "ESENTE", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getEsente() {
        return esente;
    }

    public void setEsente(BigDecimal esente) {
        this.esente = esente;
    }

    @Basic
    @Column(name = "TOTALE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getTotale() {
        return totale;
    }

    public void setTotale(BigDecimal totale) {
        this.totale = totale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TotaleFatturaAEntity that = (TotaleFatturaAEntity) o;

        if (idtotale != that.idtotale) return false;
        if (imponibile1 != null ? !imponibile1.equals(that.imponibile1) : that.imponibile1 != null) return false;
        if (aliquota1 != null ? !aliquota1.equals(that.aliquota1) : that.aliquota1 != null) return false;
        if (imposta1 != null ? !imposta1.equals(that.imposta1) : that.imposta1 != null) return false;
        if (imponibile2 != null ? !imponibile2.equals(that.imponibile2) : that.imponibile2 != null) return false;
        if (aliquota2 != null ? !aliquota2.equals(that.aliquota2) : that.aliquota2 != null) return false;
        if (imposta2 != null ? !imposta2.equals(that.imposta2) : that.imposta2 != null) return false;
        if (imponibile3 != null ? !imponibile3.equals(that.imponibile3) : that.imponibile3 != null) return false;
        if (aliquota3 != null ? !aliquota3.equals(that.aliquota3) : that.aliquota3 != null) return false;
        if (imposta3 != null ? !imposta3.equals(that.imposta3) : that.imposta3 != null) return false;
        if (imponibile != null ? !imponibile.equals(that.imponibile) : that.imponibile != null) return false;
        if (scperc != null ? !scperc.equals(that.scperc) : that.scperc != null) return false;
        if (sconto != null ? !sconto.equals(that.sconto) : that.sconto != null) return false;
        if (imponibiles != null ? !imponibiles.equals(that.imponibiles) : that.imponibiles != null) return false;
        if (imposta != null ? !imposta.equals(that.imposta) : that.imposta != null) return false;
        if (esente != null ? !esente.equals(that.esente) : that.esente != null) return false;
        if (totale != null ? !totale.equals(that.totale) : that.totale != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idtotale;
        result = 31 * result + (imponibile1 != null ? imponibile1.hashCode() : 0);
        result = 31 * result + (aliquota1 != null ? aliquota1.hashCode() : 0);
        result = 31 * result + (imposta1 != null ? imposta1.hashCode() : 0);
        result = 31 * result + (imponibile2 != null ? imponibile2.hashCode() : 0);
        result = 31 * result + (aliquota2 != null ? aliquota2.hashCode() : 0);
        result = 31 * result + (imposta2 != null ? imposta2.hashCode() : 0);
        result = 31 * result + (imponibile3 != null ? imponibile3.hashCode() : 0);
        result = 31 * result + (aliquota3 != null ? aliquota3.hashCode() : 0);
        result = 31 * result + (imposta3 != null ? imposta3.hashCode() : 0);
        result = 31 * result + (imponibile != null ? imponibile.hashCode() : 0);
        result = 31 * result + (scperc != null ? scperc.hashCode() : 0);
        result = 31 * result + (sconto != null ? sconto.hashCode() : 0);
        result = 31 * result + (imponibiles != null ? imponibiles.hashCode() : 0);
        result = 31 * result + (imposta != null ? imposta.hashCode() : 0);
        result = 31 * result + (esente != null ? esente.hashCode() : 0);
        result = 31 * result + (totale != null ? totale.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumns({@JoinColumn(name = "CODICEFORNITORE", referencedColumnName = "CODICEFORNITORE"), @JoinColumn(name = "NUMERO", referencedColumnName = "NUMERO"), @JoinColumn(name = "DATA", referencedColumnName = "DATA"), @JoinColumn(name = "TIPO", referencedColumnName = "TIPO")})
    public FatturaAEntity getFatturaA() {
        return fatturaA;
    }

    public void setFatturaA(FatturaAEntity fatturaA) {
        this.fatturaA = fatturaA;
    }
}
