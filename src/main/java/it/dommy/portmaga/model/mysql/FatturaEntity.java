package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "fattura")
public class FatturaEntity implements Model {

    private int idfattura;

    private ClienteEntity clienteEntity;

    private Integer codiceagente;

    private Integer numero;

    private String codiceletterale;

    private Date data;

    private String numerazione;

    private String destinazione;

    private String causale;

    private String trasporto;

    private String vettore;

    private String aspetto;

    private String numerocolli;

    private String peso;

    private String porto;

    private Date dataritiro;

    private Time oraritiro;

    private Integer codicepagamento;

    private String modalitapagamento;

    private BigDecimal scperc;

    private String listino;

    private String banca;

    private String tipo;

    private String note;

    private Integer idrif;

    private String rif;

    private String operatore;

    @Id
    @GeneratedValue
    @Column(name = "IDFATTURA", nullable = false)
    public int getIdfattura() {
        return idfattura;
    }

    public void setIdfattura(int idfattura) {
        this.idfattura = idfattura;
    }

    @ManyToOne
    @JoinColumn(name = "CODICECLIENTE")
    public ClienteEntity getClienteEntity() {
        return clienteEntity;
    }

    public void setClienteEntity(ClienteEntity clienteEntity) {
        this.clienteEntity = clienteEntity;
    }

    @Basic
    @Column(name = "CODICEAGENTE", nullable = true, insertable = true, updatable = true)
    public Integer getCodiceagente() {
        return codiceagente;
    }

    public void setCodiceagente(Integer codiceagente) {
        this.codiceagente = codiceagente;
    }

    @Basic
    @Column(name = "NUMERO", nullable = true, insertable = true, updatable = true)
    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Basic
    @Column(name = "CODICELETTERALE", nullable = true, insertable = true, updatable = true, length = 6)
    public String getCodiceletterale() {
        return codiceletterale;
    }

    public void setCodiceletterale(String codiceletterale) {
        this.codiceletterale = codiceletterale;
    }

    @Basic
    @Column(name = "NUMERAZIONE", nullable = true, insertable = true, updatable = true, length = 30)
    public String getNumerazione() {
        return numerazione;
    }

    public void setNumerazione(String numerazione) {
        this.numerazione = numerazione;
    }

    @Basic
    @Column(name = "DATA", nullable = true, insertable = true, updatable = true)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Basic
    @Column(name = "DESTINAZIONE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getDestinazione() {
        return destinazione;
    }

    public void setDestinazione(String destinazione) {
        this.destinazione = destinazione;
    }

    @Basic
    @Column(name = "CAUSALE", nullable = true, insertable = true, updatable = true, length = 50)
    public String getCausale() {
        return causale;
    }

    public void setCausale(String causale) {
        this.causale = causale;
    }

    @Basic
    @Column(name = "TRASPORTO", nullable = true, insertable = true, updatable = true, length = 12)
    public String getTrasporto() {
        return trasporto;
    }

    public void setTrasporto(String trasporto) {
        this.trasporto = trasporto;
    }

    @Basic
    @Column(name = "VETTORE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getVettore() {
        return vettore;
    }

    public void setVettore(String vettore) {
        this.vettore = vettore;
    }

    @Basic
    @Column(name = "ASPETTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getAspetto() {
        return aspetto;
    }

    public void setAspetto(String aspetto) {
        this.aspetto = aspetto;
    }

    @Basic
    @Column(name = "NUMEROCOLLI", nullable = true, insertable = true, updatable = true, length = 25)
    public String getNumerocolli() {
        return numerocolli;
    }

    public void setNumerocolli(String numerocolli) {
        this.numerocolli = numerocolli;
    }

    @Basic
    @Column(name = "PESO", nullable = true, insertable = true, updatable = true, length = 15)
    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    @Basic
    @Column(name = "PORTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getPorto() {
        return porto;
    }

    public void setPorto(String porto) {
        this.porto = porto;
    }

    @Basic
    @Column(name = "DATARITIRO", nullable = true, insertable = true, updatable = true)
    public Date getDataritiro() {
        return dataritiro;
    }

    public void setDataritiro(Date dataritiro) {
        this.dataritiro = dataritiro;
    }

    @Basic
    @Column(name = "ORARITIRO", nullable = true, insertable = true, updatable = true)
    public Time getOraritiro() {
        return oraritiro;
    }

    public void setOraritiro(Time oraritiro) {
        this.oraritiro = oraritiro;
    }


    @Basic
    @Column(name = "CODICEPAGAMENTO", nullable = true, insertable = true, updatable = true)
    public Integer getCodicepagamento() {
        return codicepagamento;
    }

    public void setCodicepagamento(Integer codicepagamento) {
        this.codicepagamento = codicepagamento;
    }


    @Basic
    @Column(name = "MODALITAPAGAMENTO", nullable = true, insertable = true, updatable = true, length = 100)
    public String getModalitapagamento() {
        return modalitapagamento;
    }

    public void setModalitapagamento(String modalitapagamento) {
        this.modalitapagamento = modalitapagamento;
    }



    @Basic
    @Column(name = "SCPERC", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getScperc() {
        return scperc;
    }

    public void setScperc(BigDecimal scperc) {
        this.scperc = scperc;
    }

    @Basic
    @Column(name = "LISTINO", nullable = true, insertable = true, updatable = true, length = 20)
    public String getListino() {
        return listino;
    }

    public void setListino(String listino) {
        this.listino = listino;
    }



    @Basic
    @Column(name = "BANCA", nullable = true, insertable = true, updatable = true, length = 200)
    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }


    @Basic
    @Column(name = "TIPO", nullable = true, insertable = true, updatable = true, length = 6)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }


    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    @Basic
    @Column(name = "IDRIF", nullable = true, insertable = true, updatable = true)
    public Integer getIdrif() {
        return idrif;
    }

    public void setIdrif(Integer idrif) {
        this.idrif = idrif;
    }



    @Basic
    @Column(name = "RIF", nullable = true, insertable = true, updatable = true, length = 100)
    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }



    @Basic
    @Column(name = "OPERATORE", nullable = true, insertable = true, updatable = true, length = 31)
    public String getOperatore() {
        return operatore;
    }

    public void setOperatore(String operatore) {
        this.operatore = operatore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FatturaEntity that = (FatturaEntity) o;

        if (idfattura != that.idfattura) return false;
        if (codiceagente != null ? !codiceagente.equals(that.codiceagente) : that.codiceagente != null) return false;
        if (numero != null ? !numero.equals(that.numero) : that.numero != null) return false;
        if (codiceletterale != null ? !codiceletterale.equals(that.codiceletterale) : that.codiceletterale != null)
            return false;
        if (numerazione != null ? !numerazione.equals(that.numerazione) : that.numerazione != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (destinazione != null ? !destinazione.equals(that.destinazione) : that.destinazione != null) return false;
        if (causale != null ? !causale.equals(that.causale) : that.causale != null) return false;
        if (trasporto != null ? !trasporto.equals(that.trasporto) : that.trasporto != null) return false;
        if (vettore != null ? !vettore.equals(that.vettore) : that.vettore != null) return false;
        if (aspetto != null ? !aspetto.equals(that.aspetto) : that.aspetto != null) return false;
        if (numerocolli != null ? !numerocolli.equals(that.numerocolli) : that.numerocolli != null) return false;
        if (peso != null ? !peso.equals(that.peso) : that.peso != null) return false;
        if (porto != null ? !porto.equals(that.porto) : that.porto != null) return false;
        if (dataritiro != null ? !dataritiro.equals(that.dataritiro) : that.dataritiro != null) return false;
        if (oraritiro != null ? !oraritiro.equals(that.oraritiro) : that.oraritiro != null) return false;
        if (codicepagamento != null ? !codicepagamento.equals(that.codicepagamento) : that.codicepagamento != null)
            return false;
        if (modalitapagamento != null ? !modalitapagamento.equals(that.modalitapagamento) : that.modalitapagamento != null)
            return false;
        if (scperc != null ? !scperc.equals(that.scperc) : that.scperc != null) return false;
        if (listino != null ? !listino.equals(that.listino) : that.listino != null) return false;
        if (banca != null ? !banca.equals(that.banca) : that.banca != null) return false;
        if (tipo != null ? !tipo.equals(that.tipo) : that.tipo != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (idrif != null ? !idrif.equals(that.idrif) : that.idrif != null) return false;
        if (rif != null ? !rif.equals(that.rif) : that.rif != null) return false;
        if (operatore != null ? !operatore.equals(that.operatore) : that.operatore != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idfattura;
        result = 31 * result + (codiceagente != null ? codiceagente.hashCode() : 0);
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        result = 31 * result + (codiceletterale != null ? codiceletterale.hashCode() : 0);
        result = 31 * result + (numerazione != null ? numerazione.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (destinazione != null ? destinazione.hashCode() : 0);
        result = 31 * result + (causale != null ? causale.hashCode() : 0);
        result = 31 * result + (trasporto != null ? trasporto.hashCode() : 0);
        result = 31 * result + (vettore != null ? vettore.hashCode() : 0);
        result = 31 * result + (aspetto != null ? aspetto.hashCode() : 0);
        result = 31 * result + (numerocolli != null ? numerocolli.hashCode() : 0);
        result = 31 * result + (peso != null ? peso.hashCode() : 0);
        result = 31 * result + (porto != null ? porto.hashCode() : 0);
        result = 31 * result + (dataritiro != null ? dataritiro.hashCode() : 0);
        result = 31 * result + (oraritiro != null ? oraritiro.hashCode() : 0);
        result = 31 * result + (codicepagamento != null ? codicepagamento.hashCode() : 0);
        result = 31 * result + (modalitapagamento != null ? modalitapagamento.hashCode() : 0);
        result = 31 * result + (scperc != null ? scperc.hashCode() : 0);
        result = 31 * result + (listino != null ? listino.hashCode() : 0);
        result = 31 * result + (banca != null ? banca.hashCode() : 0);
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (idrif != null ? idrif.hashCode() : 0);
        result = 31 * result + (rif != null ? rif.hashCode() : 0);
        result = 31 * result + (operatore != null ? operatore.hashCode() : 0);
        return result;
    }
}
