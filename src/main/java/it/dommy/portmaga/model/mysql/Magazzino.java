package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;

/**
 * Created by dommy on 8/7/15.
 */
@Entity
@Table(name = "magazzino")
public class Magazzino implements Model{

    private int codiceMagazzino;
    private String descrizione;


    @Id
    @Column(name ="CODICEMAGAZZINO")
    @GeneratedValue
    public int getCodiceMagazzino() {
        return codiceMagazzino;
    }

    public void setCodiceMagazzino(int codiceMagazzino) {
        this.codiceMagazzino = codiceMagazzino;
    }

    @Column(name = "descrizione")
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
}
