package it.dommy.portmaga.model.mysql;

import javax.persistence.*;

/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "categoria_merceologica")
public class CategoriaMerceologicaEntity {
    private int codicemerceologico;
    private String descrizione;

    @Id
    @Column(name = "CODICEMERCEOLOGICO", nullable = false, insertable = true, updatable = true)
    public int getCodicemerceologico() {
        return codicemerceologico;
    }

    public void setCodicemerceologico(int codicemerceologico) {
        this.codicemerceologico = codicemerceologico;
    }

    @Basic
    @Column(name = "DESCRIZIONE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoriaMerceologicaEntity that = (CategoriaMerceologicaEntity) o;

        if (codicemerceologico != that.codicemerceologico) return false;
        if (descrizione != null ? !descrizione.equals(that.descrizione) : that.descrizione != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codicemerceologico;
        result = 31 * result + (descrizione != null ? descrizione.hashCode() : 0);
        return result;
    }
}
