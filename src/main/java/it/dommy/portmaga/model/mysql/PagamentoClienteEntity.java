package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "pagamento_cliente")
public class PagamentoClienteEntity implements Model {
    private int idpagamento;

    private DocumentoVenditaEntity documentoVenditaEntity;

    private  ClienteEntity clienteEntity;

    @Id
    @GeneratedValue
    @Column(name = "IDPAGAMENTO", nullable = false)
    public int getIdpagamento() {
        return idpagamento;
    }

    public void setIdpagamento(int idpagamento) {
        this.idpagamento = idpagamento;
    }

    @ManyToOne
    @JoinColumn(name = "IDDOCUMENTO")
    public DocumentoVenditaEntity getDocumentoVenditaEntity() {
        return documentoVenditaEntity;
    }

    public void setDocumentoVenditaEntity(DocumentoVenditaEntity documentoVenditaEntity) {
        this.documentoVenditaEntity = documentoVenditaEntity;
    }

    @ManyToOne
    @JoinColumn(name = "CODICECLIENTE")
    public ClienteEntity getClienteEntity() {
        return clienteEntity;
    }

    public void setClienteEntity(ClienteEntity clienteEntity) {
        this.clienteEntity = clienteEntity;
    }
    

    private Date data;

    @Basic
    @Column(name = "DATA", nullable = true, insertable = true, updatable = true)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    private String operazione;

    @Basic
    @Column(name = "OPERAZIONE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getOperazione() {
        return operazione;
    }

    public void setOperazione(String operazione) {
        this.operazione = operazione;
    }

    private String movimento;

    @Basic
    @Column(name = "MOVIMENTO", nullable = true, insertable = true, updatable = true, length = 20)
    public String getMovimento() {
        return movimento;
    }

    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    private String banca;

    @Basic
    @Column(name = "BANCA", nullable = true, insertable = true, updatable = true, length = 100)
    public String getBanca() {
        return banca;
    }

    public void setBanca(String banca) {
        this.banca = banca;
    }

    private String numeroconto;

    @Basic
    @Column(name = "NUMEROCONTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getNumeroconto() {
        return numeroconto;
    }

    public void setNumeroconto(String numeroconto) {
        this.numeroconto = numeroconto;
    }

    private String mezzopagamento;

    @Basic
    @Column(name = "MEZZOPAGAMENTO", nullable = true, insertable = true, updatable = true, length = 50)
    public String getMezzopagamento() {
        return mezzopagamento;
    }

    public void setMezzopagamento(String mezzopagamento) {
        this.mezzopagamento = mezzopagamento;
    }

    private String riferimento;

    @Basic
    @Column(name = "RIFERIMENTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getRiferimento() {
        return riferimento;
    }

    public void setRiferimento(String riferimento) {
        this.riferimento = riferimento;
    }

    private Date scadenza;

    @Basic
    @Column(name = "SCADENZA", nullable = true, insertable = true, updatable = true)
    public Date getScadenza() {
        return scadenza;
    }

    public void setScadenza(Date scadenza) {
        this.scadenza = scadenza;
    }

    private BigDecimal importo;

    @Basic
    @Column(name = "IMPORTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImporto() {
        return importo;
    }

    public void setImporto(BigDecimal importo) {
        this.importo = importo;
    }

    private String pagamento;

    @Basic
    @Column(name = "PAGAMENTO", nullable = true, insertable = true, updatable = true, length = 15)
    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    private Date datapagamento;

    @Basic
    @Column(name = "DATAPAGAMENTO", nullable = true, insertable = true, updatable = true)
    public Date getDatapagamento() {
        return datapagamento;
    }

    public void setDatapagamento(Date datapagamento) {
        this.datapagamento = datapagamento;
    }

    private String numerodocumento;

    @Basic
    @Column(name = "NUMERODOCUMENTO", nullable = true, insertable = true, updatable = true, length = 30)
    public String getNumerodocumento() {
        return numerodocumento;
    }

    public void setNumerodocumento(String numerodocumento) {
        this.numerodocumento = numerodocumento;
    }

    private Date datadocumento;

    @Basic
    @Column(name = "DATADOCUMENTO", nullable = true, insertable = true, updatable = true)
    public Date getDatadocumento() {
        return datadocumento;
    }

    public void setDatadocumento(Date datadocumento) {
        this.datadocumento = datadocumento;
    }

    private String tipodocumento;

    @Basic
    @Column(name = "TIPODOCUMENTO", nullable = true, insertable = true, updatable = true, length = 6)
    public String getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    private String note;

    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


}
