package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by dommy on 8/8/15.
 */
@Entity
@Table(name = "totale_fattura")
public class TotaleFatturaEntity implements Model {

    private int idtotale;

    private FatturaEntity fatturaEntity;

    private ClienteEntity clienteEntity;

    private BigDecimal imponibile1;

    private BigDecimal aliquota1;

    private BigDecimal imposta1;

    private BigDecimal imponibile2;

    private BigDecimal aliquota2;

    private BigDecimal imposta2;

    private BigDecimal imponibile3;

    private BigDecimal aliquota3;

    private BigDecimal imposta3;

    private BigDecimal scperc;

    private BigDecimal imponibile;

    private BigDecimal sconto;

    private BigDecimal imponibiles;

    private BigDecimal imposta;

    private BigDecimal esente;

    private BigDecimal totale;

    private String scadenze;

    @Id
    @GeneratedValue
    @Column(name = "IDTOTALE", nullable = false)
    public int getIdtotale() {
        return idtotale;
    }

    public void setIdtotale(int idtotale) {
        this.idtotale = idtotale;
    }

    @OneToOne
    @JoinColumn(name = "IDFATTURA")
    public FatturaEntity getFatturaEntity() {
        return fatturaEntity;
    }

    public void setFatturaEntity(FatturaEntity fatturaEntity) {
        this.fatturaEntity = fatturaEntity;
    }

    @ManyToOne
    @JoinColumn(name = "CODICECLIENTE")
    public ClienteEntity getClienteEntity() {
        return clienteEntity;
    }

    public void setClienteEntity(ClienteEntity clienteEntity) {
        this.clienteEntity = clienteEntity;
    }


    @Basic
    @Column(name = "IMPONIBILE1", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile1() {
        return imponibile1;
    }

    public void setImponibile1(BigDecimal imponibile1) {
        this.imponibile1 = imponibile1;
    }


    @Basic
    @Column(name = "ALIQUOTA1", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota1() {
        return aliquota1;
    }

    public void setAliquota1(BigDecimal aliquota1) {
        this.aliquota1 = aliquota1;
    }


    @Basic
    @Column(name = "IMPOSTA1", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta1() {
        return imposta1;
    }

    public void setImposta1(BigDecimal imposta1) {
        this.imposta1 = imposta1;
    }

    @Basic
    @Column(name = "IMPONIBILE2", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile2() {
        return imponibile2;
    }

    public void setImponibile2(BigDecimal imponibile2) {
        this.imponibile2 = imponibile2;
    }


    @Basic
    @Column(name = "ALIQUOTA2", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota2() {
        return aliquota2;
    }

    public void setAliquota2(BigDecimal aliquota2) {
        this.aliquota2 = aliquota2;
    }


    @Basic
    @Column(name = "IMPOSTA2", nullable = true, insertable = true, updatable = true,precision = 15,scale = 2)
    public BigDecimal getImposta2() {
        return imposta2;
    }

    public void setImposta2(BigDecimal imposta2) {
        this.imposta2 = imposta2;
    }


    @Basic
    @Column(name = "IMPONIBILE3", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile3() {
        return imponibile3;
    }

    public void setImponibile3(BigDecimal imponibile3) {
        this.imponibile3 = imponibile3;
    }


    @Basic
    @Column(name = "ALIQUOTA3", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getAliquota3() {
        return aliquota3;
    }

    public void setAliquota3(BigDecimal aliquota3) {
        this.aliquota3 = aliquota3;
    }

    @Basic
    @Column(name = "IMPOSTA3", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta3() {
        return imposta3;
    }

    public void setImposta3(BigDecimal imposta3) {
        this.imposta3 = imposta3;
    }


    @Basic
    @Column(name = "IMPONIBILE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibile() {
        return imponibile;
    }

    public void setImponibile(BigDecimal imponibile) {
        this.imponibile = imponibile;
    }


    @Basic
    @Column(name = "SCPERC", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getScperc() {
        return scperc;
    }

    public void setScperc(BigDecimal scperc) {
        this.scperc = scperc;
    }


    @Basic
    @Column(name = "SCONTO", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getSconto() {
        return sconto;
    }

    public void setSconto(BigDecimal sconto) {
        this.sconto = sconto;
    }



    @Basic
    @Column(name = "IMPONIBILES", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImponibiles() {
        return imponibiles;
    }

    public void setImponibiles(BigDecimal imponibiles) {
        this.imponibiles = imponibiles;
    }



    @Basic
    @Column(name = "IMPOSTA", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getImposta() {
        return imposta;
    }

    public void setImposta(BigDecimal imposta) {
        this.imposta = imposta;
    }



    @Basic
    @Column(name = "ESENTE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getEsente() {
        return esente;
    }

    public void setEsente(BigDecimal esente) {
        this.esente = esente;
    }



    @Basic
    @Column(name = "TOTALE", nullable = true, insertable = true, updatable = true, precision = 15,scale = 2)
    public BigDecimal getTotale() {
        return totale;
    }

    public void setTotale(BigDecimal totale) {
        this.totale = totale;
    }



    @Basic
    @Column(name = "SCADENZE", nullable = true, insertable = true, updatable = true, length = 150)
    public String getScadenze() {
        return scadenze;
    }

    public void setScadenze(String scadenze) {
        this.scadenze = scadenze;
    }


}
