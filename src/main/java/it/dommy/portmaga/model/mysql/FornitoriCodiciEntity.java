package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;

/**
 * Created by dommy on 12/8/15.
 */
@Entity
@Table(name = "FornitoriCodici", catalog = "carpino")
public class FornitoriCodiciEntity implements Model {
    private int id;
    private String codProdDitta;
    private String codice;
    private String codiceDitta;
    private String nomeDitta;
    private String prezzo;
    private String sconto1;
    private String sconto2;
    private String sconto3;

    @Id
    @Column(name = "Id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CodProdDitta", nullable = true, insertable = true, updatable = true, length = 255)
    public String getCodProdDitta() {
        return codProdDitta;
    }

    public void setCodProdDitta(String codProdDitta) {
        this.codProdDitta = codProdDitta;
    }

    @Basic
    @Column(name = "Codice", nullable = true, insertable = true, updatable = true, length = 255)
    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    @Basic
    @Column(name = "CodiceDitta", nullable = true, insertable = true, updatable = true, length = 255)
    public String getCodiceDitta() {
        return codiceDitta;
    }

    public void setCodiceDitta(String codiceDitta) {
        this.codiceDitta = codiceDitta;
    }

    @Basic
    @Column(name = "NomeDitta", nullable = true, insertable = true, updatable = true, length = 255)
    public String getNomeDitta() {
        return nomeDitta;
    }

    public void setNomeDitta(String nomeDitta) {
        this.nomeDitta = nomeDitta;
    }

    @Basic
    @Column(name = "Prezzo", nullable = true, insertable = true, updatable = true, length = 255)
    public String getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(String prezzo) {
        this.prezzo = prezzo;
    }

    @Basic
    @Column(name = "Sconto1", nullable = true, insertable = true, updatable = true, length = 255)
    public String getSconto1() {
        return sconto1;
    }

    public void setSconto1(String sconto1) {
        this.sconto1 = sconto1;
    }

    @Basic
    @Column(name = "Sconto2", nullable = true, insertable = true, updatable = true, length = 255)
    public String getSconto2() {
        return sconto2;
    }

    public void setSconto2(String sconto2) {
        this.sconto2 = sconto2;
    }

    @Basic
    @Column(name = "Sconto3", nullable = true, insertable = true, updatable = true, length = 255)
    public String getSconto3() {
        return sconto3;
    }

    public void setSconto3(String sconto3) {
        this.sconto3 = sconto3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FornitoriCodiciEntity that = (FornitoriCodiciEntity) o;

        if (id != that.id) return false;
        if (codProdDitta != null ? !codProdDitta.equals(that.codProdDitta) : that.codProdDitta != null) return false;
        if (codice != null ? !codice.equals(that.codice) : that.codice != null) return false;
        if (codiceDitta != null ? !codiceDitta.equals(that.codiceDitta) : that.codiceDitta != null) return false;
        if (nomeDitta != null ? !nomeDitta.equals(that.nomeDitta) : that.nomeDitta != null) return false;
        if (prezzo != null ? !prezzo.equals(that.prezzo) : that.prezzo != null) return false;
        if (sconto1 != null ? !sconto1.equals(that.sconto1) : that.sconto1 != null) return false;
        if (sconto2 != null ? !sconto2.equals(that.sconto2) : that.sconto2 != null) return false;
        if (sconto3 != null ? !sconto3.equals(that.sconto3) : that.sconto3 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (codProdDitta != null ? codProdDitta.hashCode() : 0);
        result = 31 * result + (codice != null ? codice.hashCode() : 0);
        result = 31 * result + (codiceDitta != null ? codiceDitta.hashCode() : 0);
        result = 31 * result + (nomeDitta != null ? nomeDitta.hashCode() : 0);
        result = 31 * result + (prezzo != null ? prezzo.hashCode() : 0);
        result = 31 * result + (sconto1 != null ? sconto1.hashCode() : 0);
        result = 31 * result + (sconto2 != null ? sconto2.hashCode() : 0);
        result = 31 * result + (sconto3 != null ? sconto3.hashCode() : 0);
        return result;
    }
}
