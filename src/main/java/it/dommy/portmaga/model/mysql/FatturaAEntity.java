package it.dommy.portmaga.model.mysql;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;


/**
 * Created by dommy on 9/29/15.
 */
@Entity
@Table(name = "FATTURA_A", schema = "", catalog = "carpino")
@IdClass(FatturaAEntityPK.class)
public class FatturaAEntity implements Model {
    private int codicefornitore;
    private String numero;
    private Date data;
    private Integer codicecosto;
    private Integer codicepagamento;
    private String modalitapagamento;
    private BigDecimal scperc;
    private String note;
    private String tipo;
    private Integer idrif;
    private String rif;
    private Long codiceAcquisto;

    @Id
    @Column(name = "CODICEFORNITORE", nullable = false, insertable = true, updatable = true)
    public int getCodicefornitore() {
        return codicefornitore;
    }

    public void setCodicefornitore(int codicefornitore) {
        this.codicefornitore = codicefornitore;
    }

    @Id
    @Column(name = "NUMERO", nullable = false, insertable = true, updatable = true, length = 30)
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Id
    @Column(name = "DATA", nullable = false, insertable = true, updatable = true)
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }


    @Basic
    @Column(name = "CODICEACQUISTO", nullable = true, insertable = true, updatable = true)
    public Long getCodiceAcquisto() {
        return codiceAcquisto;
    }

    public void setCodiceAcquisto(Long codiceAcquisto) {
        this.codiceAcquisto = codiceAcquisto;
    }

    @Basic
    @Column(name = "CODICECOSTO", nullable = true, insertable = true, updatable = true)
    public Integer getCodicecosto() {
        return codicecosto;
    }

    public void setCodicecosto(Integer codicecosto) {
        this.codicecosto = codicecosto;
    }

    @Basic
    @Column(name = "CODICEPAGAMENTO", nullable = true, insertable = true, updatable = true)
    public Integer getCodicepagamento() {
        return codicepagamento;
    }

    public void setCodicepagamento(Integer codicepagamento) {
        this.codicepagamento = codicepagamento;
    }

    @Basic
    @Column(name = "MODALITAPAGAMENTO", nullable = true, insertable = true, updatable = true, length = 100)
    public String getModalitapagamento() {
        return modalitapagamento;
    }

    public void setModalitapagamento(String modalitapagamento) {
        this.modalitapagamento = modalitapagamento;
    }

    @Basic
    @Column(name = "SCPERC", nullable = true, insertable = true, updatable = true, precision = 2)
    public BigDecimal getScperc() {
        return scperc;
    }

    public void setScperc(BigDecimal scperc) {
        this.scperc = scperc;
    }

    @Basic
    @Column(name = "NOTE", nullable = true, insertable = true, updatable = true, length = 100)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Id
    @Column(name = "TIPO", nullable = false, insertable = true, updatable = true, length = 6)
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "IDRIF", nullable = true, insertable = true, updatable = true)
    public Integer getIdrif() {
        return idrif;
    }

    public void setIdrif(Integer idrif) {
        this.idrif = idrif;
    }

    @Basic
    @Column(name = "RIF", nullable = true, insertable = true, updatable = true, length = 100)
    public String getRif() {
        return rif;
    }

    public void setRif(String rif) {
        this.rif = rif;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FatturaAEntity that = (FatturaAEntity) o;

        if (codicefornitore != that.codicefornitore) return false;
        if (numero != null ? !numero.equals(that.numero) : that.numero != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (codicecosto != null ? !codicecosto.equals(that.codicecosto) : that.codicecosto != null) return false;
        if (codicepagamento != null ? !codicepagamento.equals(that.codicepagamento) : that.codicepagamento != null)
            return false;
        if (modalitapagamento != null ? !modalitapagamento.equals(that.modalitapagamento) : that.modalitapagamento != null)
            return false;
        if (scperc != null ? !scperc.equals(that.scperc) : that.scperc != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (tipo != null ? !tipo.equals(that.tipo) : that.tipo != null) return false;
        if (idrif != null ? !idrif.equals(that.idrif) : that.idrif != null) return false;
        if (rif != null ? !rif.equals(that.rif) : that.rif != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = codicefornitore;
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (codicecosto != null ? codicecosto.hashCode() : 0);
        result = 31 * result + (codicepagamento != null ? codicepagamento.hashCode() : 0);
        result = 31 * result + (modalitapagamento != null ? modalitapagamento.hashCode() : 0);
        result = 31 * result + (scperc != null ? scperc.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        result = 31 * result + (idrif != null ? idrif.hashCode() : 0);
        result = 31 * result + (rif != null ? rif.hashCode() : 0);
        return result;
    }
}
