package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by dommy on 7/12/15.
 */

@Entity
@Table(name = "Prodotti")
public class Prodotti implements Model{


    @Id
    @Column(name ="codice")
    private String codice;

    @Column(name ="descrizione")
    private String descrizione;

    @Column(name ="prezzo")
    private BigDecimal prezzo;

    @Column(name ="giacenza")
    private BigDecimal giacenza;

    @Column(name ="data_prezzo")
    private Date dataPrezzo;

    @Column(name ="scorta")
    private BigDecimal scorta;

    @Column(name ="provvigione")
    private BigDecimal provvigione;

    @Column(name ="misura")
    private String misura;

    @Column(name ="iva")
    private BigDecimal iva;

    @Column(name ="listino1")
    private BigDecimal listino1;

    @Column(name ="listino2")
    private BigDecimal listino2;

    @Column(name ="listino3")
    private BigDecimal listino3;

    @Column(name ="ubicazione")
    private String ubicazione;

    @Column(name ="categoria")
    private String categoria;

    @Column(name ="codiceditta")
    private String codiceditta;

    public Prodotti(){}
    public Prodotti(String codice, String descrizione, BigDecimal prezzo, BigDecimal giacenza, Date dataPrezzo, BigDecimal scorta, BigDecimal provvigione, String misura, BigDecimal iva, BigDecimal listino1, BigDecimal listino2, BigDecimal listino3, String ubicazione, String categoria, String codiceditta) {

        this.codice = codice;
        this.descrizione = descrizione;
        this.prezzo = prezzo;
        this.giacenza = giacenza;
        this.dataPrezzo = dataPrezzo;
        this.scorta = scorta;
        this.provvigione = provvigione;
        this.misura = misura;
        this.iva = iva;
        this.listino1 = listino1;
        this.listino2 = listino2;
        this.listino3 = listino3;
        this.ubicazione = ubicazione;
        this.categoria = categoria;
        this.codiceditta = codiceditta;
    }



    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public BigDecimal getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(BigDecimal prezzo) {
        this.prezzo = prezzo;
    }

    public BigDecimal getGiacenza() {
        return giacenza;
    }

    public void setGiacenza(BigDecimal giacenza) {
        this.giacenza = giacenza;
    }

    public Date getDataPrezzo() {
        return dataPrezzo;
    }

    public void setDataPrezzo(Date dataPrezzo) {
        this.dataPrezzo = dataPrezzo;
    }

    public BigDecimal getScorta() {
        return scorta;
    }

    public void setScorta(BigDecimal scorta) {
        this.scorta = scorta;
    }

    public BigDecimal getProvvigione() {
        return provvigione;
    }

    public void setProvvigione(BigDecimal provvigione) {
        this.provvigione = provvigione;
    }

    public String getMisura() {
        return misura;
    }

    public void setMisura(String misura) {
        this.misura = misura;
    }

    public BigDecimal getIva() {
        return iva;
    }

    public void setIva(BigDecimal iva) {
        this.iva = iva;
    }

    public BigDecimal getListino1() {
        return listino1;
    }

    public void setListino1(BigDecimal listino1) {
        this.listino1 = listino1;
    }

    public BigDecimal getListino2() {
        return listino2;
    }

    public void setListino2(BigDecimal listino2) {
        this.listino2 = listino2;
    }

    public BigDecimal getListino3() {
        return listino3;
    }

    public void setListino3(BigDecimal listino3) {
        this.listino3 = listino3;
    }

    public String getUbicazione() {
        return ubicazione;
    }

    public void setUbicazione(String ubicazione) {
        this.ubicazione = ubicazione;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCodiceditta() {
        return codiceditta;
    }

    public void setCodiceditta(String codiceditta) {
        this.codiceditta = codiceditta;
    }
}
