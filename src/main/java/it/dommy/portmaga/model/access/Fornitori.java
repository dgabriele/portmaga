package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by dommy on 10/4/15.
 */
@Entity
@Table(name = "Fornitori")
public class Fornitori implements Model{

    @Id
    @Column(name="RAGIONESOCIALE")
    private String ragioneSociale;

    @Column(name="INDIRIZZO")
    private String indirizzo;

    @Column(name="CITTA")
    private String citta;

    @Column(name="TELEFONO")
    private String telefono;

    @Column(name="CODICEFISCALE")
    private String codiceFiscale;

    @Column(name="PARTITAIVA")
    private String partitaIva;

    @Column(name="CODICEFORNITORE")
    private String codiceFornitore;

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getPartitaIva() {
        return partitaIva;
    }

    public void setPartitaIva(String partitaIva) {
        this.partitaIva = partitaIva;
    }

    public String getCodiceFornitore() {
        return codiceFornitore;
    }

    public void setCodiceFornitore(String codiceFornitore) {
        this.codiceFornitore = codiceFornitore;
    }
}
