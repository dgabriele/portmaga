package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by dommy on 10/4/15.
 */
@Entity
@Table(name = "DESCRIZIONIACQUISTO")
@IdClass(DescrizioniAcquisto.class)
public class DescrizioniAcquisto implements Model,Serializable {

    @Id
    @Column(name = "CODICE",nullable = true)
    private String codice;
    @Id
    @Column(name = "DESCRIZIONE")
    private String descrizione;

    @Id
    @Column(name = "QUANTITA")
    private double quantita;

    @Id
    @Column(name = "PREZZO")
    private double prezzo;

    @Id
    @Column(name = "CODICEACQUISTO")
    private long codiceAcquisto;

    @Id
    @Column(name = "DATA")
    private Date data;

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public double getQuantita() {
        return quantita;
    }

    public void setQuantita(double quantita) {
        this.quantita = quantita;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public long getCodiceAcquisto() {
        return codiceAcquisto;
    }

    public void setCodiceAcquisto(long codiceAcquisto) {
        this.codiceAcquisto = codiceAcquisto;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DescrizioniAcquisto that = (DescrizioniAcquisto) o;

        if (Double.compare(that.quantita, quantita) != 0) return false;
        if (Double.compare(that.prezzo, prezzo) != 0) return false;
        if (codiceAcquisto != that.codiceAcquisto) return false;
        if (codice != null ? !codice.equals(that.codice) : that.codice != null) return false;
        if (descrizione != null ? !descrizione.equals(that.descrizione) : that.descrizione != null) return false;
        return !(data != null ? !data.equals(that.data) : that.data != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = codice != null ? codice.hashCode() : 0;
        result = 31 * result + (descrizione != null ? descrizione.hashCode() : 0);
        temp = Double.doubleToLongBits(quantita);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(prezzo);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (codiceAcquisto ^ (codiceAcquisto >>> 32));
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }
}
