package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by dommy on 8/12/15.
 */
@Entity
@Table(name = "FATTURE")
public class Fatture implements Model,Serializable {

    @Id
    @Column(name="NUMERO")
    private String numero;

    @Id
    @Column(name="DATA")
    private Date data;

    @Column(name="IMPONIBILE")
    private double imponibile;

    @Column(name="IMPOSTA")
    private double imposta;

    @Column(name="TOTALE")
    private double totale;

    @Column(name="ACCONTO")
    private Integer acconto;

    @Column(name="RESIDUO")
    private double residuo;

    @Column(name="ACCOMPAGNATORIA")
    private boolean accompagnatoria;

    @Column(name="DESTINAZIONEDIVERSA")
    private boolean destinazioneDiversa;

    @Column(name="AGENTE")
    private String agente;

    @Column(name="PAGATO")
    private boolean pagato;

    @Column(name="PAGAMENTO")
    private String pagamento;

    @Column(name="CLIENTE")
    private String cliente ;

    @Column(name="LISTINO")
    private Integer listino;

    @Column(name="SCONTO")
    private String sconto;

    @Column(name="TIPOPAGAMENTO")
    private String tipopagamento;

    @Column(name="RAGIONESOCIALEDIVERSA")
    private String ragioneSocialeDiversa;

    @Column(name="INDIRIZZODIVERSO")
    private String indirizzodiverso;

    @Column(name="cittadiversa")
    private String cittadiversa;

    @Column(name="CAUSALE")
    private String causale;

    @Column(name="TIPOTRASPORTO")
    private String tipotrasporto;

    @Column(name="ASPETTO")
    private String aspetto;

    @Column(name="COLLI")
    private Integer colli;

    @Column(name="DATATRASPORTO")
    private Date dataTrasporto;

    @Column(name="ORATRASPORTO")
    private Date oraTrasporto;

    @Column(name="NOTE")
    private String note;

    @Column(name="PORTO")
    private String porto;

    @Column(name="DADDT")
    private boolean daddt;

    @Column(name="DATAPAGAMENTO")
    private Date datapagamento;

    @Column(name="IMPORTOSCONTO")
    private double importosconto;

    @Column(name="FATTURATA")
    private boolean fatturata;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getImponibile() {
        return imponibile;
    }

    public void setImponibile(double imponibile) {
        this.imponibile = imponibile;
    }

    public double getImposta() {
        return imposta;
    }

    public void setImposta(double imposta) {
        this.imposta = imposta;
    }

    public double getTotale() {
        return totale;
    }

    public void setTotale(double totale) {
        this.totale = totale;
    }

    public Integer getAcconto() {
        return acconto;
    }

    public void setAcconto(Integer acconto) {
        this.acconto = acconto;
    }

    public double getResiduo() {
        return residuo;
    }

    public void setResiduo(double residuo) {
        this.residuo = residuo;
    }

    public boolean isAccompagnatoria() {
        return accompagnatoria;
    }

    public void setAccompagnatoria(boolean accompagnatoria) {
        this.accompagnatoria = accompagnatoria;
    }

    public boolean isDestinazioneDiversa() {
        return destinazioneDiversa;
    }

    public void setDestinazioneDiversa(boolean destinazioneDiversa) {
        this.destinazioneDiversa = destinazioneDiversa;
    }

    public String getAgente() {
        return agente;
    }

    public void setAgente(String agente) {
        this.agente = agente;
    }

    public boolean isPagato() {
        return pagato;
    }

    public void setPagato(boolean pagato) {
        this.pagato = pagato;
    }

    public String getPagamento() {
        return pagamento;
    }

    public void setPagamento(String pagamento) {
        this.pagamento = pagamento;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getListino() {
        return listino;
    }

    public void setListino(Integer listino) {
        this.listino = listino;
    }

    public String getSconto() {
        return sconto;
    }

    public void setSconto(String sconto) {
        this.sconto = sconto;
    }

    public String getTipopagamento() {
        return tipopagamento;
    }

    public void setTipopagamento(String tipopagamento) {
        this.tipopagamento = tipopagamento;
    }

    public String getRagioneSocialeDiversa() {
        return ragioneSocialeDiversa;
    }

    public void setRagioneSocialeDiversa(String ragioneSocialeDiversa) {
        this.ragioneSocialeDiversa = ragioneSocialeDiversa;
    }

    public String getIndirizzodiverso() {
        return indirizzodiverso;
    }

    public void setIndirizzodiverso(String indirizzodiverso) {
        this.indirizzodiverso = indirizzodiverso;
    }

    public String getCittadiversa() {
        return cittadiversa;
    }

    public void setCittadiversa(String cittadiversa) {
        this.cittadiversa = cittadiversa;
    }

    public String getCausale() {
        return causale;
    }

    public void setCausale(String causale) {
        this.causale = causale;
    }

    public String getTipotrasporto() {
        return tipotrasporto;
    }

    public void setTipotrasporto(String tipotrasporto) {
        this.tipotrasporto = tipotrasporto;
    }

    public String getAspetto() {
        return aspetto;
    }

    public void setAspetto(String aspetto) {
        this.aspetto = aspetto;
    }

    public Integer getColli() {
        return colli;
    }

    public void setColli(Integer colli) {
        this.colli = colli;
    }

    public Date getDataTrasporto() {
        return dataTrasporto;
    }

    public void setDataTrasporto(Date dataTrasporto) {
        this.dataTrasporto = dataTrasporto;
    }

    public Date getOraTrasporto() {
        return oraTrasporto;
    }

    public void setOraTrasporto(Date oraTrasporto) {
        this.oraTrasporto = oraTrasporto;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPorto() {
        return porto;
    }

    public void setPorto(String porto) {
        this.porto = porto;
    }

    public boolean isDaddt() {
        return daddt;
    }

    public void setDaddt(boolean daddt) {
        this.daddt = daddt;
    }

    public Date getDatapagamento() {
        return datapagamento;
    }

    public void setDatapagamento(Date datapagamento) {
        this.datapagamento = datapagamento;
    }

    public double getImportosconto() {
        return importosconto;
    }

    public void setImportosconto(double importosconto) {
        this.importosconto = importosconto;
    }

    public boolean isFatturata() {
        return fatturata;
    }

    public void setFatturata(boolean fatturata) {
        this.fatturata = fatturata;
    }


}
