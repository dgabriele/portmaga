package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by dommy on 10/26/15.
 */
@Entity
@Table(name = "DDTFATTURE")
@IdClass(DdtFatture.class)
public class DdtFatture implements Model,Serializable{

    @Id
    @Column(name = "numero")
    public String numero;

    @Id
    @Column(name = "data")
    public Date data;

    @Id
    @Column(name = "codiceAcquisto")
    public Long codiceAcquisto;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Long getCodiceAcquisto() {
        return codiceAcquisto;
    }

    public void setCodiceAcquisto(Long codiceAcquisto) {
        this.codiceAcquisto = codiceAcquisto;
    }
}
