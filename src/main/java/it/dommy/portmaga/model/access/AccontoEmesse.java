package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by dommy on 11/8/15.
 */
@Entity(name = "ACCONTOEMESSE")
@IdClass(AccontoEmesse.class)
public class AccontoEmesse implements Model,Serializable {

    @Id
    @Column(name = "DATA")
    private Date data;

    @Id
    @Column(name = "IMPORTO")
    private double importo;

    @Id
    @Column(name = "TIPO")
    private String tipo;

    @Id
    @Column(name = "FATTURA")
    private String fattura;

    @Id
    @Column(name = "DATAFATTURA")
    private Date datafattura;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getImporto() {
        return importo;
    }

    public void setImporto(double importo) {
        this.importo = importo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFattura() {
        return fattura;
    }

    public void setFattura(String fattura) {
        this.fattura = fattura;
    }

    public Date getDatafattura() {
        return datafattura;
    }

    public void setDatafattura(Date datafattura) {
        this.datafattura = datafattura;
    }
}
