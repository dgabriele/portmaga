package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by dommy on 10/29/15.
 */
@Entity(name = "ACCONTOACQUISTO")
@IdClass(AccontoAcquisto.class)
public class AccontoAcquisto implements Model,Serializable{

    @Id
    @Column(name = "DATA")
    private Date data;

    @Id
    @Column(name = "IMPORTO")
    private double importo;

    @Id
    @Column(name = "TIPO")
    private String tipo;

    @Id
    @Column(name = "FATTURA")
    private Long fattura;

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getImporto() {
        return importo;
    }

    public void setImporto(double importo) {
        this.importo = importo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getFattura() {
        return fattura;
    }

    public void setFattura(Long fattura) {
        this.fattura = fattura;
    }
}
