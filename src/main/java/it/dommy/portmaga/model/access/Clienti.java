package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by dommy on 8/1/15.
 */
@Entity
@Table(name = "Clienti")
public class Clienti implements Model{


    @Column(name ="RAGIONESOCIALE")
    public String ragioneSociale;

    @Column(name ="indirizzo")
    public String indirizzo;

    @Column(name ="CITTA")
    public String città;

    @Column(name ="telefono")
    public String telefono;

    @Column(name ="codicefiscale")
    public String codicefiscale;

    @Column(name ="partitaiva")
    public String partitaiva;

    @Column(name ="destinazionediversa")
    public boolean destinazionediversa;

    @Column(name ="tipopagamento")
    public String tipopagamento;

    @Column(name ="sconto")
    public String sconto;

    @Column(name ="credito")
    public double credito;

    @Column(name ="ragionesocialediversa")
    public String ragionesocialediversa;

    @Column(name ="indirizzodiverso")
    public String indirizzodiverso;

    @Column(name ="cittadiversa")
    public String cittadiversa;

    @Id
    @Column(name ="CODICECLIENTE",unique = true)
    public String codicecliente;

    @Column(name ="listino")
    public Integer listino;

    @Column(name ="tempomedio")
    public Integer tempomedio;

    @Column(name ="agente")
    public String agente;

    public String getRagioneSociale() {
        return ragioneSociale;
    }

    public void setRagioneSociale(String ragioneSociale) {
        this.ragioneSociale = ragioneSociale;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCittà() {
        return città;
    }

    public void setCittà(String città) {
        this.città = città;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCodicefiscale() {
        return codicefiscale;
    }

    public void setCodicefiscale(String codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public String getPartitaiva() {
        return partitaiva;
    }

    public void setPartitaiva(String partitaiva) {
        this.partitaiva = partitaiva;
    }

    public boolean isDestinazionediversa() {
        return destinazionediversa;
    }

    public void setDestinazionediversa(boolean destinazionediversa) {
        this.destinazionediversa = destinazionediversa;
    }

    public String getTipopagamento() {
        return tipopagamento;
    }

    public void setTipopagamento(String tipopagamento) {
        this.tipopagamento = tipopagamento;
    }

    public String getSconto() {
        return sconto;
    }

    public void setSconto(String sconto) {
        this.sconto = sconto;
    }

    public double getCredito() {
        return credito;
    }

    public void setCredito(double credito) {
        this.credito = credito;
    }

    public String getRagionesocialediversa() {
        return ragionesocialediversa;
    }

    public void setRagionesocialediversa(String ragionesocialediversa) {
        this.ragionesocialediversa = ragionesocialediversa;
    }

    public String getIndirizzodiverso() {
        return indirizzodiverso;
    }

    public void setIndirizzodiverso(String indirizzodiverso) {
        this.indirizzodiverso = indirizzodiverso;
    }

    public String getCittadiversa() {
        return cittadiversa;
    }

    public void setCittadiversa(String cittadiversa) {
        this.cittadiversa = cittadiversa;
    }

    public String getCodicecliente() {
        return codicecliente;
    }

    public void setCodicecliente(String codicecliente) {
        this.codicecliente = codicecliente;
    }

    public Integer getListino() {
        return listino;
    }

    public void setListino(Integer listino) {
        this.listino = listino;
    }

    public Integer getTempomedio() {
        return tempomedio;
    }

    public void setTempomedio(Integer tempomedio) {
        this.tempomedio = tempomedio;
    }

    public String getAgente() {
        return agente;
    }

    public void setAgente(String agente) {
        this.agente = agente;
    }
}
