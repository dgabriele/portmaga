package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by dommy on 8/12/15.
 */
@Entity
@Table(name = "DESCRIZIONIFATTURE")
public class DescrizioniFatture implements Model {

    @Column(name="NUMERO",length = 10)
    private String numero;

    @Column(name="DATA")
    private Date data;

    @Column(name="CODICE",length = 10)
    private String codice;

    @Column(name="QUANTITA")
    private double quantita;

    @Column(name="PREZZO")
    private double prezzo;

    @Column(name="IMPORTO")
    private double importo;

    @Column(name="MISURA")
    private String misura;

    @Column(name="IVA")
    private String iva;

    @Column(name="SCONTO")
    private String sconto;

    @Column(name="PROVVIGIONE")
    private double provvigione;

    @Column(name="DESCRIZIONE")
    private String descrizione;

    @Id
    @Column(name="POSIZIONE")
    private double posizione;

    @Column(name="STORNATI")
    private double stornati;

    @Column(name="LOTTO")
    private String lotto;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public double getQuantita() {
        return quantita;
    }

    public void setQuantita(double quantita) {
        this.quantita = quantita;
    }

    public double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    public double getImporto() {
        return importo;
    }

    public void setImporto(double importo) {
        this.importo = importo;
    }

    public String getMisura() {
        return misura;
    }

    public void setMisura(String misura) {
        this.misura = misura;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getSconto() {
        return sconto;
    }

    public void setSconto(String sconto) {
        this.sconto = sconto;
    }

    public double getProvvigione() {
        return provvigione;
    }

    public void setProvvigione(double provvigione) {
        this.provvigione = provvigione;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public double getPosizione() {
        return posizione;
    }

    public void setPosizione(double posizione) {
        this.posizione = posizione;
    }

    public double getStornati() {
        return stornati;
    }

    public void setStornati(double stornati) {
        this.stornati = stornati;
    }

    public String getLotto() {
        return lotto;
    }

    public void setLotto(String lotto) {
        this.lotto = lotto;
    }
}
