package it.dommy.portmaga.model.access;

import it.dommy.portmaga.model.Model;
import it.dommy.portmaga.model.mysql.FatturaAEntityPK;

import javax.persistence.*;
import java.sql.Date;


/**
 * Created by dommy on 10/4/15.
 */
@Entity
@Table(name = "Acquisto")
public class Acquisto implements Model {

    @Column(name = "NUMERO")
    private String numero;

    @Column(name = "DATA")
    private Date data;

    @Column(name = "IMPONIBILE")
    private double imponibile;

    @Column(name = "IMPOSTA")
    private double imposta;

    @Column(name = "TOTALE")
    private double totale;

    @Column(name = "ACCONTO")
    private double acconto;

    @Column(name = "residuo")
    private double residuo;

    @Column(name = "PAGATO")
    private boolean pagato;

    @Column(name = "FATTURA")
    private boolean fattura;

    @Column(name = "TIPOPAGAMENTO")
    private String tipoPagamento;

    @Column(name = "FORNITORE")
    private String fornitore;

    @Id
    @Column(name = "CODICEACQUISTO")
    private Long codiceAcquisto;

    @Column(name = "PROTOCOLLO")
    private Long protocollo;

    @Column(name = "datapagamento")
    private Date datapagamento;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getImponibile() {
        return imponibile;
    }

    public void setImponibile(double imponibile) {
        this.imponibile = imponibile;
    }

    public double getImposta() {
        return imposta;
    }

    public void setImposta(double imposta) {
        this.imposta = imposta;
    }

    public double getTotale() {
        return totale;
    }

    public void setTotale(double totale) {
        this.totale = totale;
    }

    public double getAcconto() {
        return acconto;
    }

    public void setAcconto(double acconto) {
        this.acconto = acconto;
    }

    public double getResiduo() {
        return residuo;
    }

    public void setResiduo(double residuo) {
        this.residuo = residuo;
    }

    public boolean isPagato() {
        return pagato;
    }

    public void setPagato(boolean pagato) {
        this.pagato = pagato;
    }

    public boolean isFattura() {
        return fattura;
    }

    public void setFattura(boolean fattura) {
        this.fattura = fattura;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }


    public String getFornitore() {
        return fornitore;
    }

    public void setFornitore(String fornitore) {
        this.fornitore = fornitore;
    }

    public Long getCodiceAcquisto() {
        return codiceAcquisto;
    }

    public void setCodiceAcquisto(Long codiceAcquisto) {
        this.codiceAcquisto = codiceAcquisto;
    }

    public Long getProtocollo() {
        return protocollo;
    }

    public void setProtocollo(Long protocollo) {
        this.protocollo = protocollo;
    }

    public Date getDatapagamento() {
        return datapagamento;
    }

    public void setDatapagamento(Date datapagamento) {
        this.datapagamento = datapagamento;
    }
}
